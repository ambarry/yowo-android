package com.yowoapp.alexander.yowo;

import android.content.Context;
import android.support.v7.app.AlertDialog;

public class AlertRunnable implements Runnable {
    private final String _title;
    private final String _msg;
    private final Context _context;
    public AlertRunnable(Context context, String title, String msg) {
        _title = title;
        _msg = msg;
        _context = context;
    }

    @Override
    public void run() {
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setTitle(_title).setMessage(_msg).show();
    }
}
