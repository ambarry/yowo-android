package com.yowoapp.alexander.yowo.msg;

import android.os.*;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.yowoapp.alexander.yowo.cnx.ConnectionReadCallback;
import com.yowoapp.alexander.yowo.protocol.Hap;
import com.yowoapp.alexander.yowo.util.BiKeyMapper;
import com.yowoapp.alexander.yowo.msg.encoders.ArriveEncoder;
import com.yowoapp.alexander.yowo.msg.encoders.AuthEncoder;
import com.yowoapp.alexander.yowo.msg.encoders.FollowEncoder;
import com.yowoapp.alexander.yowo.msg.encoders.InitUserEncoder;
import com.yowoapp.alexander.yowo.msg.encoders.MsgEncodeCallback;
import com.yowoapp.alexander.yowo.msg.encoders.PublishHapEncoder;
import com.yowoapp.alexander.yowo.msg.encoders.UnfollowEncoder;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


// TODO: separate the msg handling from the map UI update part
// msg handling / writing should span activities
// UI part w/ handler can be activity specific?
// then swap the handler and state processor?
// or just deal, since each task should have what it needs...
// at least for now, just deal...

/**
 * Spawns tasks to process received messages in background
 * before updating UI.
 * Manages thread pool and queue of Runnables and task objects.
 * Anything that runs in bg and reports back to UI should really
 * go through here, since it has the handler.
 */
public class MsgManager implements ConnectionReadCallback {
    // --- instance ---
    private static final MsgManager s_Instance = new MsgManager();

    // ---
    private final BlockingQueue<Runnable> _decodeRunnableQueue;
    private final BlockingQueue<Runnable> _writeRunnableQueue;
    private final Queue<MsgDecodeTask> _decodeTaskQueue;
    private final Queue<MsgWriterTask> _writeTaskQueue;
    private final ThreadPoolExecutor _decodeThreadPool;
    private final ThreadPoolExecutor _writeThreadPool;
    private static Handler _handler;

    // --- map info to provide handler
    private WeakReference<GoogleMap> _mapRef;
    private BiKeyMapper<Marker, String, Hap> _markerHaps;
    private boolean _readyForLocations;
    private LatLng _currentLocation;

    private MsgAlertCallback _callback;


    /**
     * Constructs queues of Runnables and Tasks.
     * Gets Handler.
     * Creates ThreadPool.
     * Private to prevent instantiation outside of the class.
     */
    private MsgManager(){
        _readyForLocations = false;
        _markerHaps = new BiKeyMapper<>();
        int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
        int KEEP_ALIVE_TIME = 1;
        TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

        _handler = new UpdateHandler(Looper.getMainLooper());
        _decodeRunnableQueue = new LinkedBlockingQueue<Runnable>();
        _writeRunnableQueue = new LinkedBlockingQueue<Runnable>();
        _decodeTaskQueue = new LinkedBlockingQueue<MsgDecodeTask>();
        _writeTaskQueue = new LinkedBlockingQueue<MsgWriterTask>();
        _decodeThreadPool = new ThreadPoolExecutor(
                NUMBER_OF_CORES,       // Initial pool size
                NUMBER_OF_CORES,       // Max pool size
                KEEP_ALIVE_TIME,                KEEP_ALIVE_TIME_UNIT,
                _decodeRunnableQueue);
        _writeThreadPool = new ThreadPoolExecutor(
                NUMBER_OF_CORES,       // Initial pool size
                NUMBER_OF_CORES,       // Max pool size
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                _writeRunnableQueue);
    }

    /**
     * Gets singleton instance of MsgManager.
     */
    public static MsgManager getInstance() {
        return s_Instance;
    }


    /**
     * State processor for decoded messages.
     * Controls flow.
     * Issues / Recycles tasks accordingly.
     */
    public void handleDecodeState(MsgDecodeTask task, int state) {
        switch(state) {
            case MsgState.RECV_OK:
                Log.d("[<<< MsgManager >>>]", "recv ok");
                recycleMsgDecodeTask(task); // don't need to do anything
                break;
            case MsgState.RECV_ERROR:
                // TODO: send some sort of info to handler for display and action
                Log.d("[<<< MsgManager >>>]", "recv error");
                recycleMsgDecodeTask(task);
                break;
            case MsgState.RECV_DISPLAY_MSG:
                // TODO: send some sort of info to handler for display
                Log.d("[<<< MsgManager >>>]", "recv display msg");
                recycleMsgDecodeTask(task);
                break;
            case MsgState.DECODED_HAP:
                Log.d("[<<< MsgManager >>>]", "decoded hap");
                Message newHapMsg = _handler.obtainMessage(state, task);
                newHapMsg.sendToTarget();
                break;
            case MsgState.DECODE_ERROR:
                Log.d("[<<< MsgManager >>>]", "decode error");
                recycleMsgDecodeTask(task);
                break;
            case MsgState.DECODE_DUPLICATE:
                Log.d("[<<< MsgManager >>>]", "decode duplicate");
                recycleMsgDecodeTask(task);
                break;
            case MsgState.READY_FOR_AUTH:
                Log.d("[<<< MsgManager >>>]", "authenticate");
                recycleMsgDecodeTask(task);
                authenticate("c0c07", "c0c08"); // TODO: pull for real
                break;
            case MsgState.AUTH_COMPLETE:
                Log.d("[<<< MsgManager >>>]", "init user");
                recycleMsgDecodeTask(task);
                initUser();
                break;
            case MsgState.INIT_COMPLETE:
                Log.d("[<<< MsgManager >>>]", "init complete");
                recycleMsgDecodeTask(task);
                _readyForLocations = true;
                if (_currentLocation != null) {
                    arrive(_currentLocation);
                }
                break;
            default:
                Log.d("[<<< MsgManager >>>]", "entered default state");
                recycleMsgDecodeTask(task);
                break;
        }
    }

    /**
     * State processor for write tasks.
     */
    public void handleWriteState(MsgWriterTask task, int state) {
        switch(state) {
            case MsgState.WRITE_COMPLETE:
                Log.d("[<<< MsgManager >>>]", "write complete");
                // TODO: display something about post
                recycleMsgWriteTask(task);
                break;
            case MsgState.WRITE_ERROR:
                // TODO: display error
                Log.d("[<<< MsgManager >>>]", "write error");
                recycleMsgWriteTask(task);
                break;
            default:
                break;
        }
    }

    /**
     * State processor for hap fetching thread.
     * TODO: possibly break this out of MsgManager... requires 2nd handler though...
     */
    public void handleFetchState(HapFetcherTask task, int state) {
        switch(state) {
            case MsgState.FETCHED_INITIAL_HAPS:
                Log.d("[<<< MsgManager >>>]", "got initial haps, sending to handler...");
                _handler.obtainMessage(MsgState.FETCHED_INITIAL_HAPS, task).sendToTarget();
                // NOTE: don't recycle task here! needs to be handled first!
                break;
            default:
                Log.d("[<<< MsgManager >>>]", "no initial haps");
                task.recycle();
                break;
        }
    }


    public void setMap(GoogleMap map) {
        s_Instance._mapRef = new WeakReference<>(map);
    }

    public static GoogleMap getMap() {
        if (s_Instance._mapRef != null) {
            return s_Instance._mapRef.get();
        }
        return null;
    }

    /**
     * Gets map of Marker to Haps.
     */
    @NonNull
    public static BiKeyMapper<Marker, String, Hap> getMarkerHapMap() {
        return s_Instance._markerHaps;
    }

    /**
     * Fetches initial haps in bg and posts to UI thread handler.
     */
    public void startFetchInitialHaps() {
        Log.d("MsgManager", "starting to fetch haps");
        HapFetcherTask hTask = new HapFetcherTask();
        hTask.run();
    }

    /**
     * Implements the ConnectionReadCallback.
     */
    @Override
    public void handleBuffer(ByteBuffer buffer) {
        startMsgDecode(buffer);
    }

    /**
     * Starts to decode and handle a Yowo Msg from server.
     */
    private MsgDecodeTask startMsgDecode(ByteBuffer buf) {
        MsgDecodeTask msgTask = _decodeTaskQueue.poll(); // get task from pool, or null if still empty
        if (msgTask == null) {
            msgTask = new MsgDecodeTask();
        }

        msgTask.setMsgBuffer(buf); // set buffer
        _decodeThreadPool.execute(msgTask.getMsgRunnable()); // execute on thread from pool

        return msgTask;
    }

    /**
     * Arrives at a location, through write task.
     */
    public MsgWriterTask arrive(LatLng latLng) {
        Log.d("MsgManager", "arrive");
        s_Instance._currentLocation = latLng;
        if (s_Instance._readyForLocations) {
            return startMsgWrite(new ArriveEncoder(latLng)); // this is probably failing...
        }
        else {
            return null;
        }
    }

    /**
     * Authenticates through write task.
     */
    public static MsgWriterTask authenticate(String username, String password) {
        return startMsgWrite(new AuthEncoder(username, password));
    }

    /**
     * Publishes new Hap through write task.
     */
    public static MsgWriterTask publishHap(String title, String message, LatLng latLng, String category, int duration) {
        Log.d("MsgManager", "publishing hap");
        return startMsgWrite(new PublishHapEncoder(title, message, latLng, category, duration));
    }

    /**
     * Inits user through write task.
     */
    public static MsgWriterTask initUser() {
        return startMsgWrite(new InitUserEncoder());
    }

    /**
     * Follows a user through write task.
     */
    public static MsgWriterTask follow(String userId, String username) {
        return startMsgWrite(new FollowEncoder(userId, username));
    }

    /**
     * Unfollows a user through write task.
     */
    public static MsgWriterTask unfollow(String userId, String username) {
        return startMsgWrite(new UnfollowEncoder(userId, username));
    }


//    /**
//     * Saves a location for this user.
//     */
//    private Thread saveLocation(LatLng latLng) {
//        return sendMsg(Protocol.saveLocation(latLng));
//        // TODO: add to internal state of locations
//    }
//


    /**
     * Given an encoder callback, starts a write task.
     */
    private static MsgWriterTask startMsgWrite(MsgEncodeCallback encoder) {
        MsgWriterTask msgTask = s_Instance._writeTaskQueue.poll();
        if (msgTask == null) {
            msgTask = new MsgWriterTask();
        }
        msgTask.setEncoder(encoder);
        s_Instance._writeThreadPool.execute(msgTask.getMsgRunnable());
        return msgTask;
    }


    /**
     * Cancels all Tasks.
     */
    public static void cancelAllMsgTasks() {
        // TODO: clean this up... use the base MsgTask when you get arrays to work

        int decodeSize = s_Instance._decodeRunnableQueue.size();
        int writeSize = s_Instance._writeRunnableQueue.size();
        MsgDecodeTask[] decodeTasks = new MsgDecodeTask[decodeSize];
        MsgWriterTask[] writeTasks = new MsgWriterTask[writeSize];
        s_Instance._decodeTaskQueue.toArray(decodeTasks);
        s_Instance._writeTaskQueue.toArray(writeTasks);
         // Iterates over the array of tasks and interrupts each one's Thread.
        synchronized (s_Instance) {
            for (MsgDecodeTask task : decodeTasks) {
                Thread thread = task.getCurrentThread();
                if (thread != null) {
                    thread.interrupt();
                }
            }

            for (MsgWriterTask task : writeTasks) {
                Thread thread = task.getCurrentThread();
                if (thread != null) {
                    thread.interrupt();
                }
            }
        }
    }

    /**
     * Cancels a specific Task.
     */
    public static void removeMsgTask(MsgTask msgTask) {
        if (msgTask != null) {
            synchronized (s_Instance) {
                Thread thread = msgTask.getCurrentThread();
                if (thread != null) {
                    thread.interrupt();
                }
            }
        }
    }


    public void recycleMsgDecodeTask(MsgDecodeTask task) {
        recycleMsgTask(task, _decodeTaskQueue);
    }

    public void recycleMsgWriteTask(MsgWriterTask task) {
        recycleMsgTask(task, _writeTaskQueue);
    }

    /**
     * Recycles tasks by calling their internal recycle() method and then putting them back into
     * the task queue.
     */
    private <T extends MsgTask> void recycleMsgTask (T msgTask, Queue<T> queue) {
        msgTask.recycle(); // Frees up memory in the task
        queue.offer(msgTask); // Puts the task object back into the queue for re-use.
    }
}