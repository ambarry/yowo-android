package com.yowoapp.alexander.yowo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;


public class AddHapDialogFragment extends DialogFragment {
    private View _hapDialogView;
    HapDialogListener _listener;

    public AddHapDialogFragment() {
        // Required empty public constructor
    }

    public String getInputTitle() {
        EditText txtHapTitle = (EditText)_hapDialogView.findViewById(R.id.txtHapTitle);
        return txtHapTitle.getText().toString();
    }

    public String getInputMessage() {
        EditText txtHapMessage = (EditText)_hapDialogView.findViewById(R.id.txtHapMessage);
        return txtHapMessage.getText().toString();
    }

    public String getCategory() {
        Spinner spn = (Spinner)_hapDialogView.findViewById(R.id.spnCategory);
        String cat = ((String)spn.getSelectedItem()).replace(' ', '_'); // TODO: map to hap category if you change names...;
        Log.d("HapDialog", "Spinner selected item: " + cat);
        return cat;
    }

    public int getDuration() {
        RadioGroup rdgDuration = (RadioGroup) _hapDialogView.findViewById(R.id.rdgDuration);
        int id = rdgDuration.getCheckedRadioButtonId();
        int duration;
        switch (id) {
            case (R.id.rdo5min):
                duration = 300;
                break;
            case (R.id.rdo15min):
                duration = 900;
                break;
            case (R.id.rdoHalfHour):
                duration = 1800;
                break;
            default:
                duration = 3600;
                break;
        }
        Log.d("HapDialog", "Radio group selected duration: " + String.valueOf(duration));
        return duration;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // have to use inflater to create view for min api v.19, can't just use the layout name until v.21
        //LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        _hapDialogView = inflater.inflate(R.layout.fragment_add_hap_dialog, null);
        Spinner spn =  (Spinner)_hapDialogView.findViewById(R.id.spnCategory);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.hap_categories, R.layout.support_simple_spinner_dropdown_item);
        spn.setAdapter(adapter);
        builder
            .setTitle("What's mappening?")
            .setView(_hapDialogView) // Hap lil form
            .setPositiveButton("Post!", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // DO STUFF!
                    _listener.onDialogPositiveClick(AddHapDialogFragment.this);
                }
            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // cancelled
                        _listener.onDialogNegativeClick(AddHapDialogFragment.this);
                    }
                }
        );
        return builder.create();
    }


    // Override the Fragment.onAttach() method to instantiate the HapDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Fragment frag = getTargetFragment(); // get target fragment which should implement callbacks, not the activity anymore!
        if (frag instanceof HapDialogListener) {
            _listener = (HapDialogListener) frag;
        } else {
            throw new ClassCastException(frag.toString() + " must implement HapDialogListener");
        }
    }

    /**
     * Public interface for interaction.
     * Have parent activity implement this to forward events.
     */
    public interface HapDialogListener {
        void onDialogPositiveClick(DialogFragment dialog);
        void onDialogNegativeClick(DialogFragment dialog);
    }


}
