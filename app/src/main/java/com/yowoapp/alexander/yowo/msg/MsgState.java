package com.yowoapp.alexander.yowo.msg;

public class MsgState {
    public static final int DECODED_HAP = 1;
    public static final int RECV_OK = 2;
    public static final int RECV_ERROR = 3;
    public static final int RECV_DISPLAY_MSG = 4;
    public static final int READY_FOR_AUTH = 5;
    public static final int AUTH_COMPLETE = 6;
    public static final int WRITE_COMPLETE = 9;
    public static final int WRITE_ERROR = 10;
    public static final int INIT_COMPLETE = 11;
    public static final int DECODE_ERROR = 12;
    public static final int DECODE_DUPLICATE = 13;
    public static final int FETCHED_INITIAL_HAPS = 14;
    public static final int NO_INITIAL_HAPS = 15;
}
