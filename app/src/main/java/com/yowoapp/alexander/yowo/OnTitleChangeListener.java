package com.yowoapp.alexander.yowo;

/**
 * Title change interface. MainActivity must implement.
 * Call during fragment attach.
 */
public interface OnTitleChangeListener {
    void onTitleChange(String title);
}
