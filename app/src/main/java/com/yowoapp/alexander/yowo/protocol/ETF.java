package com.yowoapp.alexander.yowo.protocol;


import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.HashMap;

public class ETF {

    public static final byte FLOAT = (byte)70;
    public static final byte SMALL_INT = (byte)97;
    public static final byte INT = (byte)98;
    public static final byte ATOM = (byte)100;
    public static final byte CHAR_LIST = (byte)107;
    public static final byte BINARY = (byte)109;
    public static final byte MAP = (byte)116;
    public static final byte SMALL_TUPLE = (byte)104;
    public static final byte LARGE_TUPLE = (byte)105;
    public static final byte SMALL_BIG_EXT = (byte)110;

    private static short intToShort(int i) {
        short s = i > Short.MAX_VALUE ? Short.MAX_VALUE : (short)i;
        return s;
    }

    public static ByteBuffer toETFSmallInt(int i) {
        //97, the int as 1 byte
        ByteBuffer buf = ByteBuffer
                .allocate(2)
                .put(SMALL_INT)
                .put((byte) i);
        buf.flip();
        return buf;
    }

    public static int decodeETFSmallInt(ByteBuffer buf) {
        // we already read the 97
        // TODO: make sure this cast works, esp with the sign...
        int i = buf.get();
        return i;
    }

    public static ByteBuffer toETFInt(int i) {
        //98, the int as 4 bytes
        ByteBuffer buf = ByteBuffer
                .allocate(5)
                .put(INT)
                .putInt(i);
        buf.flip();
        return buf;
    }

    public static int decodeETFInt(ByteBuffer buf) {
        // already read the 98
        return buf.getInt();
    }

    // NOTE: even though we're creating an Erlang float,
    // it requires 8 bytes which maps better to the Java double.
    // This also wll let us use the default lat / long props of Google's map
    // more easily.
    // Not worried about rounding.
    public static ByteBuffer toETFFloat(double d) {
        // 70
        ByteBuffer buf = ByteBuffer
                .allocate(9)
                .put(FLOAT)
                .putDouble(d);
        buf.flip();
        return buf;
    }


    public static double decodeETFFloat(ByteBuffer buf) {
        // we already read the 70
        double d = buf.getDouble();
        return d;
    }

    public static long decodeETFSmallBig(ByteBuffer buf) {
        // gregorian seconds, e.g., come through as this
        // we already ready the 100
        int len = buf.get(); // not a downcast, should be okay
        Log.d("ETF", "length: " + Integer.valueOf(len).toString());
        buf.get(); // sign: must be 0 (pos) or 1(neg), but we know it's a 0 for now...
        int b = 256;
        int tmp;
        long val = 0;
        for(int i = 0; i < len; i++) {
            tmp = buf.get();
            val += tmp * Math.pow(b, i);
        }
        return val;
        //B = 256
        //(d0*B^0 + d1*B^1 + d2*B^2 + ... d(N-1)*B^(n-1))
    }

    public static ByteBuffer toETFCharList(String str){
        // NOTE: the ETF string really comes through as char list,
        // so this has been named accordingly. Strings should use ETF.toBinary

        // TODO: need to guarantee str < 255!!!
        //107, 2 bytes length, then all chars
        short len = intToShort(str.length());

        ByteBuffer buf = ByteBuffer
                .allocate(3 + len)
                .put(CHAR_LIST)
                .putShort(len)
                .put(str.getBytes(StandardCharsets.UTF_8));
        buf.flip();
        return buf;
    }

    public static String decodeETFCharList(ByteBuffer buf) {
        // already read the 107
        short len = buf.getShort();
        byte[] bytes = new byte[len];
        buf.get(bytes);
        return new String(bytes, StandardCharsets.UTF_8);
    }

    public static ByteBuffer toETFBinary(String str) {
        int len = str.length();
        ByteBuffer buf = ByteBuffer
                .allocate(5 + len)
                .put(BINARY)
                .putInt(len)
                .put(str.getBytes(StandardCharsets.UTF_8));
        buf.flip();
        return buf;
    }

    public static String decodeETFBinary(ByteBuffer buf) {
        // at this point, we've already read the 109 and know it's a binary
        // need to read length, then parse into string and return.
        // this will keep moving the buffer's current position, considering
        // it was created by wrapping the full array.
        int len = buf.getInt();
        byte[] bytes = new byte[len];
        buf.get(bytes);
        return new String(bytes, StandardCharsets.UTF_8);
    }

    public static ByteBuffer toETFAtom(String str) {
        // TODO: need to guarantee str < 255!!!
        // 100, 2 bytes len (max 255), atom name chars
        short len = intToShort(str.length());
        ByteBuffer buf = ByteBuffer
                .allocate(3 + len)
                .put(ATOM)
                .putShort(len)
                .put(str.getBytes(StandardCharsets.UTF_8));
        buf.flip();
        return buf;
    }

    public static String decodeETFAtom(ByteBuffer buf) {
        // already read the 100
        short len = buf.getShort();
        Log.d("ETF", "decode atom length: " + Short.valueOf(len).toString());
        byte[] bytes = new byte[len];
        buf.get(bytes);
        return new String(bytes, StandardCharsets.UTF_8);
    }

    public static ByteBuffer toETFSmallTuple(int numElements, ByteBuffer etfElements) {
        // 104 + byte of numElements + elements
        ByteBuffer buf = ByteBuffer
                .allocate(2 + etfElements.capacity())
                .put(SMALL_TUPLE)
                .put((byte)numElements)
                .put(etfElements);
        buf.flip();
        return buf;
    }

    public static Object[] decodeETFSmallTuple(ByteBuffer buf) {
        // we already have the 131 and the 104
        int numElements = buf.get(); // TODO: make sure cast works...
        Object[] elements = new Object[numElements];
        for (int i = 0; i < numElements; i++) {
            elements[i] = decodeETFObject(buf);
        }
        return elements;
    }

    public static ByteBuffer toETFMap(int numPairs, ByteBuffer etfPairs) {
        // 116, arity in 4 bytes, the pairs as k1, v1, k2, v2, ... kn, vn
        // no duplicate keys allowed!!!
        ByteBuffer buf = ByteBuffer
                .allocate(5 + etfPairs.capacity())
                .put(MAP)
                .putInt(numPairs)
                .put(etfPairs);
        buf.flip();
        return buf;
    }

    public static Map<String, Object> decodeETFMap(ByteBuffer buf) {
        // we already have the 131 and 116
        int numPairs = buf.getInt();
        Map<String, Object> map = new HashMap<>(numPairs);
        String key;
        Object val;
        //byte tag;
        Log.d("ETF", "map pairs = " + Integer.valueOf(numPairs).toString());

        for (int i = 0; i < numPairs; i++) {
            //tag = buf.get(); // TODO: this SHOULD be an atom, but may need to check tag??
            buf.get();
            key = decodeETFAtom(buf);
            Log.d("ETF", "key: " + key);
            val = decodeETFObject(buf);
            Log.d("ETF", "val: " + val);
            map.put(key, val);
        }
        return map;
    }


    public static Object decodeETFObject(ByteBuffer buf) {
        Object res;
        byte tag = buf.get();


        switch(tag) {
            case ATOM:
                res = decodeETFAtom(buf);
                break;
            case BINARY:
                res = decodeETFBinary(buf);
                break;
            case CHAR_LIST:
                res = decodeETFCharList(buf);
                break;
            case FLOAT:
                res = decodeETFFloat(buf);
                break;
            case INT:
                res = decodeETFInt(buf);
                break;
            case MAP:
                res = decodeETFMap(buf);
                break;
            case SMALL_INT:
                res = decodeETFSmallInt(buf);
                break;
            case SMALL_TUPLE:
                res = decodeETFSmallTuple(buf);
                break;
            case SMALL_BIG_EXT:
                res = decodeETFSmallBig(buf);
                break;
            default:
                Log.d("ETF", "unhandled tag: " + Byte.valueOf(tag).toString());
                res = null;
                break;
        }
        return res;
    }
}
