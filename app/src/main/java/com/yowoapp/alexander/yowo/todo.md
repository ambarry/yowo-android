# TODO

## connection
- kill callbacks on stop (although, just MainActivity)
- connect on startup, warn if not connected
    + this needs to disable actions or possibly the app....
    + launch from MainActivity
    + change map frag to not start connection but if it has one / check auth status, then init / syncs
- broadcast receiver
    + handle changes to connection state
    + if offline, don't try to send messages
    + either store for syncing, or alert user
    
## location service
- detect
    + alert if not
    + ability to turn it on
- start listening if active
- send "arrive" on real changes

## login
- get info on login
- store
- check on startup when tokens in place
- refresh token...

## follows
[ ] can't follow self!!!
   - must hide / change msg (this is me!) and disable button
[ ] need to fetch / sync follows
[ ] list fragment
[ ] handle errors from server (need more detail, code or msg or something)
    [ ] UI
    [ ] some things should have actions, like if following a user has failed, a retry or tie-breaker
    
## location
[ ] change schema
    + name or desc
    + real coords (for marker)
    + approx coords (for subscribing) -> unless we want to round each time
[ ] list fragment
[ ] sync on startup


---


## minor issues
- typing should let you advance to next section in AddHapDialog
- menu issue -> double settings

## profile
- info
- pic
- fragment

## notifications
[ ] Notifications on new haps / markers, regardless of whether running in bg or during app only
[ ] should open map activity and move camera to marker on click

## validation
[ ] set sizes of messages, etc.

## map UI
[ ] ultimate form of marker layout...
    - title gets cut off (don't forget)
[ ] set icon based on category (will eventually need some sleek graphics, could see if J is interested...)
[ ] clustering map icons instead of the 5 limit / position adjustment
    - need to adjust save then as well...

## images...
[ ] storing / loading / decoding
[ ] caching / expiration -> although, do we need sep call to retrieve?
    * probably, as we don't want to jam up back end...

## app events
[ ] handle starts, pauses, loads, etc.

## dbUpgrade
* make sure this works properly...