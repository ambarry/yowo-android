package com.yowoapp.alexander.yowo;

/**
 * Enum of actions to take from MarkerDialogFragment.
 */
public enum MarkerDialogAction {
    MARK_DONE,
    FOLLOW_USER,
    UNFOLLOW_USER,
    SAVE_LOCATION,
    BLOCK_USER,
    CLOSE
}
