package com.yowoapp.alexander.yowo.msg;

import com.yowoapp.alexander.yowo.protocol.Hap;

import java.nio.ByteBuffer;

/**
 * Maintains a sort of state of message processing Runnable,
 * as well as data and view.
 * Notifies MsgManager upon completion.
 */
public class MsgDecodeTask extends MsgTask {
    private Hap _hap;
    private ByteBuffer _buffer;

    /**
     * Constructs new MsgDecoderRunnable.
     */
    MsgDecodeTask() {
        runnable = new MsgDecoderRunnable(this); // create runnable
    }

    /**
     * Handles state reported from decode runnable.
     * Reports state out to MsgManager.
     */
    public void handleDecodeState(int state) {
        // TODO: can handle some commands that need new runnables, like ready_for_auth, etc.
        handleState(state);
    }

    /**
     * Gets msg buffer.
     */
    public ByteBuffer getMsgBuffer(){
        return _buffer;
    }

    /**
     * Sets msg buffer.
     */
    public void setMsgBuffer(ByteBuffer buf) {
        _buffer = buf;
    }

    /**
     * Gets the Hap.
     */
    public Hap getHap() {
        return _hap;
    }

    /**
     * Sets the Hap after decoding.
     */
    public void setHap(Hap hap) {
        _hap = hap;
    }

    /**
     * Sends state and task (this) to MsgManager.
     */
    @Override
    protected void handleState(int state) {
        s_MsgManager.handleDecodeState(this, state);
    }

    /**
     * Clean up and avoid leaks.
     */
    @Override
    public void recycle() {
        // Releases references
        _buffer = null;
        _hap = null;
    }
}
