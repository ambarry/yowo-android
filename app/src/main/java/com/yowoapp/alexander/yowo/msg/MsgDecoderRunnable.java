package com.yowoapp.alexander.yowo.msg;

import android.util.Log;

import com.yowoapp.alexander.yowo.db.DbHelper;
import com.yowoapp.alexander.yowo.protocol.Hap;
import com.yowoapp.alexander.yowo.protocol.ETF;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Processes new messages from server.
 */
public class MsgDecoderRunnable implements Runnable {
    private MsgDecodeTask _decodeTask;

    /**
     * Sets up command map from string to enum.
     */
    static final Map<String, Integer> _stages;
    static {
        HashMap<String, Integer> stages = new HashMap<>();
        stages.put("READY_FOR_AUTH", MsgState.READY_FOR_AUTH);
        stages.put("AUTH_COMPLETE", MsgState.AUTH_COMPLETE);
        stages.put("INIT_COMPLETE", MsgState.INIT_COMPLETE);
        _stages = Collections.unmodifiableMap(stages);
    }

    /**
     * Constructs runnable w/ instance of task.
     */
    MsgDecoderRunnable(MsgDecodeTask task) {
        _decodeTask = task;
    }

    /**
     * Runs decoding, first settings task to background and grabbing ref
     * of thread for interruption.
     */
    public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND); // run in background
        _decodeTask.setCurrentThread(Thread.currentThread()); // so it can be interrupted
        decodeBuffer(_decodeTask.getMsgBuffer());
    }

    /**
     * Decodes a ByteBuffer into proper msg / state.
     */
    private void decodeBuffer(ByteBuffer buf) {

        // make sure this wasn't interrupted before continuing
        if (Thread.interrupted()) {
            return;
        }

        String message;
        Map<String, Object> map;
        int state = MsgState.DECODE_ERROR;
        Log.d("[<<< MsgDecRun >>>]", "about to read buffer");
        buf.get(); // 131
        Log.d("[<<< MsgDecRun >>>]", "got 131");
        byte b = buf.get(); // get ETF tag
        Log.d("[<<< MsgDecRun >>>]", "got ETF tag");
        switch (b) {
            case ETF.ATOM:
                // TODO: this is old, now using StageResult struct
                message = ETF.decodeETFAtom(buf);
                Log.d("[<<< MsgDecRun >>>]", "received atom: " + message);
                if (_stages.containsKey(message)){
                    state = _stages.get(message);
                }
                break;
            case ETF.BINARY:
                message = ETF.decodeETFBinary(buf); // not really using yet...
                Log.d("[<<< MsgDecRun >>>]", "received string or binary: " + message);
                state = MsgState.RECV_DISPLAY_MSG;
                break;

            case ETF.MAP:
                Log.d("[<<< MsgDecRun >>>]", "received map, about to parse...");
                map = ETF.decodeETFMap(buf);
                if (isStruct(map)) {
                    Log.d("[<<< MsgDecRun >>>]", "is struct");
                    if (isHap(map)) {
                        Log.d("[<<< MsgDecRun >>>]", "is hap");
                        Hap hap = new Hap(map);
                        DbHelper s_DbHelper = DbHelper.getInstance();
                        hap = s_DbHelper.insertHap(s_DbHelper.getWritableDatabase(), hap);
                        if (hap != null) {
                            Log.d("[<<< MsgDecRun >>>]", "got hap");
                            _decodeTask.setHap(hap);
                            state = MsgState.DECODED_HAP;
                        }
                        else {
                            Log.d("[<<< MsgDecRun >>>]", "got duplicate hap");
                            state = MsgState.DECODE_DUPLICATE;
                        }
                    }
                    else if (isStageResult(map)) {
                        Log.d("[<<< MsgDecRun >>>]", "is stage result");
                        String stage = (String)map.get("stage");
                        if (_stages.containsKey(stage)) {
                            state = _stages.get(stage);
                            // TODO: if state was loginresult, get data %{user_id: id, username: name}
                        }
                        else {
                            Log.d("[<<< MsgDecRun >>>]", "unknown stage result treating as error...");
                        }
                    }
                } else if (map.containsKey("ok")) {
                    Log.d("[<<< MsgDecRun >>>]", "is OK");
                    //String command = (String) map.get("ok"); // not really using yet, could do instead of states...
                    state = MsgState.RECV_OK;
                } else if (map.containsKey("error")) {
                    Log.d("[<<< MsgDecRun >>>]", "is ERROR");
                    //String command = (String) map.get("error");
                    // TODO: set error on task for display, when protocol is established
                    state = MsgState.DECODE_ERROR;
                }
                break;
            default:
                Log.d("[<<< MsgDecRun >>>]", "failed to decode ETF type -> reporting error...");
                break;
        }
        _decodeTask.handleDecodeState(state);
    }

    private boolean isStruct(Map<String, Object> map) {
        return map.containsKey("__struct__");
    }

    private boolean isStageResult(Map<String, Object> map) {
        return (map.get("__struct__")).equals("Elixir.Yowo.StageResult");
    }

    /**
     * Checks that the received map is a Hap struct.
     */
    private boolean isHap(Map<String, Object> map) {
        return (map.get("__struct__")).equals("Elixir.Yowo.Hap");
    }
}
