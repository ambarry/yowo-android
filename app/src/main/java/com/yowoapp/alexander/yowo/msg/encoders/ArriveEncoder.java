package com.yowoapp.alexander.yowo.msg.encoders;

import com.google.android.gms.maps.model.LatLng;
import com.yowoapp.alexander.yowo.protocol.Protocol;

import java.nio.ByteBuffer;

/**
 * Creates msg encoder callback
 * for arriving at a location.
 */
public class ArriveEncoder implements MsgEncodeCallback {
    private LatLng _latLng;

    public ArriveEncoder(LatLng latLng) {
        _latLng = latLng;
    }

    @Override
    public ByteBuffer encodeMsg() {
        return Protocol.arrive(_latLng);
    }
}
