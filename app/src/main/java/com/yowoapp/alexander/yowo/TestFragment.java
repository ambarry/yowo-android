package com.yowoapp.alexander.yowo;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class TestFragment extends Fragment {
    private static final String SOURCE_PARAM = "ARG_SOURCE";
    private String source;
    private OnTitleChangeListener _listener;

    public TestFragment() {
        // Required empty public constructor
    }

    public static TestFragment newInstance(String source) {
        TestFragment fragment = new TestFragment();
        Bundle args = new Bundle();
        args.putString(SOURCE_PARAM, source);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            source = getArguments().getString(SOURCE_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (_listener != null) {
            _listener.onTitleChange("Welcome");
        }
        View view = inflater.inflate(R.layout.fragment_test, container, false);
        TextView txtSource = (TextView)view.findViewById(R.id.txtSource);
        String sourceText = "Blank fragment created from: " + source;
        txtSource.setText(sourceText);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTitleChangeListener) {
            _listener = (OnTitleChangeListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTitleChangeListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d("[<<< TestFragment >>>]", "onDetach");
        super.onDetach();
        _listener = null;
    }
}
