package com.yowoapp.alexander.yowo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * TODO: this may need to be it's own activity, or else block nav drawer...
 */
public class LoginFragment extends Fragment implements View.OnClickListener {
    private OnLoginFragmentListener _listener;

    public LoginFragment() {
        // Required empty public constructor
    }

    // pass in args if needed, bundle, set frag args
    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //if (getArguments() != null) {}

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        Button btnLogIn = (Button) view.findViewById(R.id.btnLogIn);
        btnLogIn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        Log.d("[<<< LogIn Frag >>>]", "click");
        if (_listener != null) {
            // TODO:
            _listener.onLogIn();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginFragmentListener) {
            _listener = (OnLoginFragmentListener) context;
            _listener.onTitleChange("Sign In");
        } else {
            throw new RuntimeException(context.toString() + " must implement OnLoginFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        _listener = null;
    }

    public interface OnLoginFragmentListener extends OnTitleChangeListener {
        // TODO: pass in user stuff? how to we receive success from MsgManager?
        void onLogIn();
    }
}
