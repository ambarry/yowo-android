package com.yowoapp.alexander.yowo.msg.encoders;

import com.yowoapp.alexander.yowo.protocol.Protocol;

import java.nio.ByteBuffer;

/**
 * Creates msg encoder callback
 * for following a user.
 */
public class FollowEncoder implements MsgEncodeCallback {
    private String _userId;
    private String _username;

    public FollowEncoder(String userId, String username) {
        _userId = userId;
        _username = username;
    }

    @Override
    public ByteBuffer encodeMsg() {
        return Protocol.follow(_userId, _username);
    }


}
