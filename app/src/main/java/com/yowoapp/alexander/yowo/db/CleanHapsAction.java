package com.yowoapp.alexander.yowo.db;

/**
 * Cleans expired haps.
 */
public class CleanHapsAction implements DbActionCallback {
    public CleanHapsAction() {}

    @Override
    public void doAction() {
        DbHelper s_dbHelper = DbHelper.getInstance();
        s_dbHelper.cleanExpiredHaps(s_dbHelper.getWritableDatabase());
    }
}
