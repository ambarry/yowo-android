package com.yowoapp.alexander.yowo;

import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;


/**
 * Re-exposing the drawer close event from toggle since
 * the delay is making the fragment load janky.
 */
public class CloseTheDamnDrawer extends ActionBarDrawerToggle {
    private final OnDamnDrawerClosed _closer;

    public CloseTheDamnDrawer(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar,
                              int openDrawerContentDescRes, int closeDrawerContentDescRes, OnDamnDrawerClosed closer) {
        super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        _closer = closer;
    }

    @Override
    public void onDrawerClosed(View drawerView){
        super.onDrawerClosed(drawerView);
        if (_closer != null){
            _closer.onDamnDrawerClosed(drawerView);
        }
    }

    public interface OnDamnDrawerClosed {
        void onDamnDrawerClosed(View drawerView);
    }
}