package com.yowoapp.alexander.yowo.msg;

public interface MsgAlertCallback {
    void handleAlert(String title, String message);
}
