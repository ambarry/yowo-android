package com.yowoapp.alexander.yowo;

import android.app.Dialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yowoapp.alexander.yowo.cnx.ConnectionManager;
import com.yowoapp.alexander.yowo.db.DbActionRunnable;
import com.yowoapp.alexander.yowo.db.FollowUserAction;
import com.yowoapp.alexander.yowo.db.MarkHapDoneAction;
import com.yowoapp.alexander.yowo.db.UnfollowUserAction;
import com.yowoapp.alexander.yowo.msg.MsgManager;
import com.yowoapp.alexander.yowo.protocol.Hap;
import com.yowoapp.alexander.yowo.util.BiKeyMapper;


/**
 * The map fragment. A wrapper around the actual GoogleMapFragment
 * that can be swapped in and out and is self-contained.
 * Implements necessary callbacks for map and related actions.
 * Updates toolbar and title on parent activity.
 */
public class YowoMapFragment extends Fragment implements
        OnMapReadyCallback,
        MarkerDialogFragment.OnMarkerDialogListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnInfoWindowLongClickListener,
        AddHapDialogFragment.HapDialogListener {

    private OnYowoMapFragmentListener _listener;
    private GoogleMap _map;
    private Location _lastKnownLocation;
    private LocationListener _locationListener;
    private MsgManager s_MsgManager;

    public YowoMapFragment() {
        // Required empty public constructor
    }

    public static YowoMapFragment newInstance() {
        YowoMapFragment fragment = new YowoMapFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("[<<< MapFrag >>>]", "onCreateView");
        setHasOptionsMenu(true); // need to update title and action bar
        View view =  inflater.inflate(R.layout.fragment_yowo_map, container,false);
        if (_listener != null) {
            _listener.onTitleChange("Your World");
        }
        s_MsgManager = MsgManager.getInstance();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Log.d("[<<< MapFrag >>>]", "onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        // get map
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.d("[<<< MapFrag >>>]", "onCreateOptionsMenu");
        inflater.inflate(R.menu.map_menu, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Log.d("[<<< MapFrag >>>]", "onOptionsItemSelected");
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                return false; // in activity
            case R.id.action_add_hap:
                showHapDialog();
                return true;
            case R.id.action_connect:
                ConnectionManager cnx = ConnectionManager.getInstance();
                cnx.setReadCallback(s_MsgManager); // forward msgs
                cnx.startConnection();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onAttach(Context context){
        Log.d("[<<< MapFrag >>>]", "onAttach");
        super.onAttach(context);
        try {
            _listener = (OnYowoMapFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnYowoMapFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d("[<<< MapFrag >>>]", "onDetach");
        super.onDetach();
        _listener = null;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("[<<< MapFrag >>>]", "onMapReady");
        _map = googleMap;
        _map.setOnInfoWindowClickListener(this);
        _map.setOnInfoWindowLongClickListener(this);
        _map.setInfoWindowAdapter(new HapInfoWindowAdapter(getActivity().getLayoutInflater(), MsgManager.getMarkerHapMap()));

        setUpLocationListener();

        // TODO: deal w/ null _lastKnownLocation, e.g. location turned off... need to show and display a msg, optioanlly turn on service

        // set map on msg manager
        s_MsgManager.setMap(_map);
        s_MsgManager.startFetchInitialHaps(); // NOTE: may not want to do here, since this is called each time we return to fragment!
        s_MsgManager.arrive(getLatLng(_lastKnownLocation)); // will arrive if ready, otherwise just set loc

        moveCamera(getLatLng(_lastKnownLocation), 15.0F);

        // then, start reading now?
        //s_MsgManager.startConnection();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Log.d("[<<< MapFrag >>>]", "onInfoWindowClick");
        marker.hideInfoWindow();
    }

    @Override
    public void onInfoWindowLongClick(Marker marker) {
        Log.d("[<<< MapFrag >>>]", "onInfoWindowLongClick");
        Hap hap = MsgManager.getMarkerHapMap().get(marker); // this seems stupid, getting id just to get marker again...
        MarkerDialogFragment dialog = MarkerDialogFragment.newInstance(hap.hapId);
        dialog.setTargetFragment(this, 1); // make sure it dispatches callbacks here, not activity!
        dialog.show(getFragmentManager(), "fragment_marker_dialog");
    }

    @Override
    public void onMarkerDialogListen(Dialog dialog, MarkerDialogAction action, Hap hap) {
        Log.d("[<<< MapFrag >>>]", "onMarkerDialogListen");
        // new threads for the db writes may not be worth it...
        switch (action) {
            case MARK_DONE:
                Log.d("MainActivity", "mark done");
                dialog.dismiss();
                BiKeyMapper<Marker, String, Hap> markerHaps = MsgManager.getMarkerHapMap();
                Marker marker = markerHaps.getByValueKey(hap.hapId);
                markerHaps.remove(marker);
                marker.remove();
                new Thread(new DbActionRunnable(new MarkHapDoneAction(hap.hapId))).start();
                break;
            case FOLLOW_USER:
                Log.d("MainActivity", "follow user");
                new Thread(new DbActionRunnable(new FollowUserAction(hap.userId, hap.username))).start();
                MsgManager.follow(hap.userId, hap.username);
                break;
            case UNFOLLOW_USER:
                Log.d("MainActivity", "unfollow user");
                new Thread(new DbActionRunnable(new UnfollowUserAction(hap.userId))).start();
                MsgManager.unfollow(hap.userId, hap.username);
                break;
            case SAVE_LOCATION:
                // TODO:
                // use msg manager to send (from hap info)
                // update toggle optimistically?
                // need to handle remove?
                // may not even want to do from here...
                // the saves should also be general loc..., need to round
                break;
            default:
                dialog.dismiss();
                break;
        }
    }

    /**
     * Displays the Add Hap dialog.
     */
    public void showHapDialog() {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new AddHapDialogFragment();
        dialog.setTargetFragment(this, 1); // use this to set callback, forget the attach!
        dialog.show(getFragmentManager(), "hap_adder");
    }

    /**
     * Handles the positive click callback of the Add Hap dialog.
     * The dialog fragment receives a reference to this Activity through the
     * Fragment.onAttach() callback, which it uses to call the following methods
     * defined by the NoticeDialogFragment.NoticeDialogListener interface
     */
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        // TODO: need to actually check if we're connected, otherwise store locally, eventually retry, or display msg
        AddHapDialogFragment hapDialog = (AddHapDialogFragment)dialog;
        MsgManager.publishHap(hapDialog.getInputTitle(), hapDialog.getInputMessage(), getLatLng(_lastKnownLocation), hapDialog.getCategory(), hapDialog.getDuration());
    }

    /**
     * Handles the negative click callback of the Add Hap dialog.
     */
    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // TODO: do we need to do anything, close dialog?
        // doesn't seem like it...
    }

    /**
     * Sets up Location Listener for changes to a User's location.
     * This needs to be adjusted to handle service denial, and
     * be optimized for battery performance.
     * Should send an ARRIVE msg to Yowo server when location has
     * changed enough.
     */
    private void setUpLocationListener() {
        Log.d("[<<< MapFrag >>>]", "setupLocationListener");
        LocationManager locationManager = (LocationManager)getContext().getSystemService(Context.LOCATION_SERVICE);
        try
        {
            _lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        catch (SecurityException ex)
        {
            // TODO: display some sort of error...
            Log.d("Location", "Can't get last known location: security exception.");
        }
        _locationListener = new YowoLocationListener();

        // NOT DOING YET! Let's just try to receive first...
        //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 0, _LocationListener);
    }

    /**
     * Convenience method.
     * Creates a LatLng from a Location.
     */
    private LatLng getLatLng(Location location) {
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

    /**
     * Moves the map camera.
     */
    private void moveCamera(LatLng latLng, float zoom) {
        _map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }


    /**
     * Kill messages and db reads going on.
     */
    @Override
    public void onStop() {
        Log.d("[<<< MapFrag >>>]", "onStop");
        // Cancel all the running threads managed by the MsgManager
        MsgManager.cancelAllMsgTasks(); // may need to stop earlier if coming from YowoMapFragment...
        //ConnectionManager.cancelRead();
        super.onStop();
    }

    /**
     * Adds marker to map.
     * Must be called / run on UI thread.
     */
    public static void addNewMarker(GoogleMap map, BiKeyMapper<Marker, String, Hap> markerHapMap, Hap hap) {
        if (map == null) {
            return;
        }

        Log.d("UpdateHandler", "adding new marker");
        // TODO: set icon here (in opts though)
        //marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.zorac));
        // TODO: check position and hapId against current hashmap for marker display
        // although right now this is already handled during save...
        Marker marker = map.addMarker(new MarkerOptions().position(new LatLng(hap.lat, hap.lng)));
        markerHapMap.put(marker, hap.hapId, hap); // this doesn't have to be on ui
    }

    /**
     * Interface to implement from caller.
     * Extend as needed.
     */
    public interface OnYowoMapFragmentListener extends OnTitleChangeListener {

    }

}
