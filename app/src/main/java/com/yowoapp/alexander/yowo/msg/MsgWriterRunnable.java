package com.yowoapp.alexander.yowo.msg;

import android.util.Log;

import com.yowoapp.alexander.yowo.cnx.ConnectionManager;
import com.yowoapp.alexander.yowo.msg.encoders.MsgEncodeCallback;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

/**
 * Handles writes to server.
 */
public class MsgWriterRunnable implements Runnable {
    private MsgWriterTask _task;
    public MsgWriterRunnable(MsgWriterTask task) {
        _task = task;
    }

    public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
        _task.setCurrentThread(Thread.currentThread());
        try {
            writeToElixir();
            _task.handleWriteState(MsgState.WRITE_COMPLETE);
        } catch (IOException iex) {
            _task.handleWriteState(MsgState.WRITE_ERROR);
            Log.d("Writer", iex.getMessage());
        }
    }

    private void writeToElixir() throws IOException {
        WritableByteChannel channel = ConnectionManager.getInstance().getChannel();
        MsgEncodeCallback encoder = _task.getEncoder();
        ByteBuffer msg = encoder.encodeMsg();
        ByteBuffer header = ByteBuffer
                .allocate(5) // packet 4: 4-byte msg length before all TCP messages
                .putInt(msg.limit() + 1)
                .put((byte)131);
        header.flip();

        // lock before write
        synchronized (MsgLock.LOCK) {
            channel.write(header);
            channel.write(msg);
        }
    }
}
