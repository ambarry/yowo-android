package com.yowoapp.alexander.yowo.msg;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yowoapp.alexander.yowo.YowoMapFragment;
import com.yowoapp.alexander.yowo.protocol.Hap;
import com.yowoapp.alexander.yowo.util.BiKeyMapper;

/**
 * Updates the UI thread.
 */
public class UpdateHandler extends Handler {
    public UpdateHandler(Looper looper) {
        super(looper);
    }

    @Override
    public void handleMessage(Message msg) {
        BiKeyMapper<Marker, String, Hap> hapMap = MsgManager.getMarkerHapMap();
        GoogleMap map = MsgManager.getMap();
        switch(msg.what) {
            case MsgState.FETCHED_INITIAL_HAPS:
                Log.d("[<<< UpdateHandler >>>]", "received FETCHED_INITIAL_HAPS");
                HapFetcherTask hapTask = (HapFetcherTask)msg.obj;
                for(Hap hap : hapTask.getHaps()){
                    YowoMapFragment.addNewMarker(map, hapMap, hap);
                }
                hapTask.recycle();
                break;
            case MsgState.DECODED_HAP:
                Log.d("[<<< UpdateHandler >>>]", "received DECODE_HAP");
                MsgDecodeTask task = (MsgDecodeTask) msg.obj;
                YowoMapFragment.addNewMarker(map, hapMap, task.getHap());
                MsgManager.getInstance().recycleMsgDecodeTask(task);
                task.recycle();
                break;
            // TODO: may want to separate state for when pic is available ultimately, and
            // re show the map marker if already created?
            default:
                // TODO: handle display msgs and display errors.
                // use a regular alert dialog?
                super.handleMessage(msg);
                break;
        }
    }
}