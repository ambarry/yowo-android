package com.yowoapp.alexander.yowo.cnx;

import java.nio.ByteBuffer;

/**
 * Basic interface for handling msgs from Connection.
 */
public interface ConnectionReadCallback {
    void handleBuffer(ByteBuffer buf);
}
