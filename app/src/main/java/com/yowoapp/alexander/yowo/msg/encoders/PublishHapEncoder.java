package com.yowoapp.alexander.yowo.msg.encoders;

import com.google.android.gms.maps.model.LatLng;
import com.yowoapp.alexander.yowo.protocol.Hap;
import com.yowoapp.alexander.yowo.protocol.Protocol;

import java.nio.ByteBuffer;

/**
 * Creates msg encode callback for
 * publishing a new hap.
 */
public class PublishHapEncoder implements MsgEncodeCallback {
    private String _title;
    private String _message;
    private LatLng _latLng;
    private String _category;
    private int _duration;

    public PublishHapEncoder(String title, String message, LatLng latLng, String category, int duration) {
        _title = title;
        _message = message;
        _latLng = latLng;
        _category = category;
        _duration = duration;
    }

    @Override
    public ByteBuffer encodeMsg() {
        return Protocol.publishHap(new Hap(_title, _message, _latLng.latitude, _latLng.longitude, _category, _duration));
    }
}
