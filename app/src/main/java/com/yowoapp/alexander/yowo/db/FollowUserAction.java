package com.yowoapp.alexander.yowo.db;

/**
 * Follows a user in db.
 */
public class FollowUserAction implements DbActionCallback {
    String _userId;
    String _username;

    public FollowUserAction(String userId, String username) {
        _userId = userId;
        _username = username;
    }

    @Override
    public void doAction() {
        DbHelper dbHelper = DbHelper.getInstance();
        dbHelper.follow(dbHelper.getWritableDatabase(), _userId, _username);
    }
}
