package com.yowoapp.alexander.yowo;

import android.app.Dialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ToggleButton;

import com.yowoapp.alexander.yowo.db.DbHelper;
import com.yowoapp.alexander.yowo.protocol.Hap;
import com.yowoapp.alexander.yowo.msg.MsgManager;


/**
 * Marker dialog fragment w/ options.
 */
public class MarkerDialogFragment
    extends DialogFragment
    implements View.OnClickListener {

    private Hap _hap;
    private boolean _isFollowing;
    private boolean _hasLocation;
    private OnMarkerDialogListener _listener;
    private ToggleButton _togFollows;
    private ToggleButton _togSaveLocation;


    public MarkerDialogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * NOTE: args for bundle must be Parceable, so don't try to use whole hap
     * or you could lose it during activity lifecycle and dialog recreation.
     * It is better to pull from activity during onCreate.
     */
    public static MarkerDialogFragment newInstance(String hapId) {
        MarkerDialogFragment fragment = new MarkerDialogFragment();
        Bundle args = new Bundle();

        args.putString("hapId", hapId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String hapId = getArguments().getString("hapId");
            _hap = MsgManager.getMarkerHapMap().getValueByValueKey(hapId);
        }
        // if this becomes a problem, do in bg
        DbHelper dbHelper = DbHelper.getInstance();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        _isFollowing = dbHelper.isFollowing(db, _hap.userId);
        _hasLocation = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View markerDialogView = inflater.inflate(R.layout.fragment_marker_dialog, container, false);
        getDialog().setTitle("What would you like to do?");

        // TODO: if we need more room, could display more info here
        // or have sep. dialog for the short click!!! maybe just title, user, appraisal, or truncated text on actual map

        // --- find and set buttons ---
        Button btnClose = (Button) markerDialogView.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);

        Button btnMarkDone = (Button) markerDialogView.findViewById(R.id.btnMarkDone);
        btnMarkDone.setOnClickListener(this);

        _togFollows = (ToggleButton)markerDialogView.findViewById(R.id.togFollowUser);
        _togFollows.setTextOn(getFollowTextOn(_hap.username));
        _togFollows.setTextOff(getFollowTextOff(_hap.username));
        _togFollows.setChecked(_isFollowing);
        _togFollows.setOnClickListener(this);

        // TODO: detect if location already saved!!! NOTE: should save approx? actually, may not want to from here anyway...
        _togSaveLocation = (ToggleButton)markerDialogView.findViewById(R.id.togSaveLocation);
        _togSaveLocation.setTextOn("Location saved");
        _togSaveLocation.setTextOff("Follow this location?");
        _togSaveLocation.setChecked(false);
        _togSaveLocation.setOnClickListener(this);

        return markerDialogView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Fragment frag = getTargetFragment();
        if (frag instanceof OnMarkerDialogListener) {
            _listener = (OnMarkerDialogListener) frag;
        } else {
            throw new RuntimeException(frag.toString() + " must implement OnMarkerDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        _listener = null;
    }

    @Override
    public void onClick(View v) {
        // figure out view, take action
        int id = v.getId();

        MarkerDialogAction action;
        switch (id) {
            case R.id.btnMarkDone:
                Log.d("MarkerDialog", "mark done");
                action = MarkerDialogAction.MARK_DONE;
                break;
            case R.id.togFollowUser:
                Log.d("MarkerDialog", "follow / unfollow");
                _isFollowing = _togFollows.isChecked();
                action = _isFollowing ? MarkerDialogAction.FOLLOW_USER: MarkerDialogAction.UNFOLLOW_USER;
                break;
            case R.id.togSaveLocation:
                action = MarkerDialogAction.SAVE_LOCATION;
                break;
            case R.id.btnClose:
                action = MarkerDialogAction.CLOSE;
                break;
            default:
                action = MarkerDialogAction.CLOSE;
                break;
        }
        // do we even need to do this? could just handle in frag...
        // ideally, more of a centralized presenter class? basically the msg manager for now, but would be nice to split out
        _listener.onMarkerDialogListen(getDialog(), action, _hap);
    }

    /**
     * Public interface for interaction.
     * Have parent activity implement this to forward events.
     */
    public interface OnMarkerDialogListener {
        void onMarkerDialogListen(Dialog dialog, MarkerDialogAction action, Hap hap);
    }

    private String getFollowTextOn(String username) {
        return "Following " + username;
    }

    private String getFollowTextOff(String username) {
        return "Follow " + username + "?";
    }

}
