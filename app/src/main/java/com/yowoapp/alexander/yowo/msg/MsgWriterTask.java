package com.yowoapp.alexander.yowo.msg;


import com.yowoapp.alexander.yowo.msg.encoders.MsgEncodeCallback;

public class MsgWriterTask extends MsgTask {
    private MsgEncodeCallback _encoder;


    /**
     * Constructs a new MsgWriterRunnable.
     */
    MsgWriterTask() {
        runnable = new MsgWriterRunnable(this);
    }

    /**
     * Handles state reported by write Runnable.
     * Reports state out to MsgManager.
     */
    public void handleWriteState(int state) {
        // do things if needed...
        handleState(state);
    }

    /**
     * Gets the encode callback implementer.
     */
    public MsgEncodeCallback getEncoder() {
        return _encoder;
    }

    /**
     * Sets the encode callback implementer.
     */
    public void setEncoder(MsgEncodeCallback encoder) {
        _encoder = encoder;
    }


    /**
     * Sends state and task (this) to MsgManager.
     */
    @Override
    protected void handleState(int state) {
        s_MsgManager.handleWriteState(this, state);
    }

    /**
     * Clean up and avoid leaks.
     */
    @Override
    public void recycle() {
        _encoder = null;
    }

}
