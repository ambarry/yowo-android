package com.yowoapp.alexander.yowo.msg.encoders;

import com.yowoapp.alexander.yowo.protocol.Protocol;

import java.nio.ByteBuffer;

/**
 * Creates msg encode callback for
 * authentication.
 */
public class AuthEncoder implements MsgEncodeCallback {
    private String _username;
    private String _password;

    public AuthEncoder(String username, String password){
        _username = username;
        _password = password;
    }

    @Override
    public ByteBuffer encodeMsg() {
        return Protocol.authenticate(_username, _password);
    }
}
