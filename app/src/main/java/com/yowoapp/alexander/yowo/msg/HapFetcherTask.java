package com.yowoapp.alexander.yowo.msg;

import android.util.Log;

import com.yowoapp.alexander.yowo.protocol.Hap;

/**
 * Simple task to fetch initial haps.
 */
public class HapFetcherTask extends MsgTask {
    private Hap[] _haps;

    public HapFetcherTask() {
        runnable = new HapFetcherRunnable(this);
    }

    public void run() {
        Log.d("HapFetcherTask", "starting run...");
        runThread = new Thread(runnable);
        runThread.start();
    }

    @Override
    public void handleState(int state) {
        s_MsgManager.handleFetchState(this, state);
    }

    public void setHaps(Hap[] haps) {
        _haps = haps;
    }

    public Hap[] getHaps() {
        return _haps;
    }

    @Override
    public void recycle() {
        _haps = null;
    }
}
