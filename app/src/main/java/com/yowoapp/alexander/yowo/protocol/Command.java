package com.yowoapp.alexander.yowo.protocol;

public enum Command {
    AUTHENTICATE,
    ARRIVE,
    BLOCK_USER,
    FOLLOW,
    INIT_USER,
    PUBLISH_HAP,
    REGISTER,
    REMOVE_LOCATION,
    SAVE_LOCATION,
    UNFOLLOW,
}