package com.yowoapp.alexander.yowo.protocol;

import com.yowoapp.alexander.yowo.util.HasBiKey;

import java.util.Date;
import java.util.Map;

public class Hap implements HasBiKey<String> {
    public String hapId;
    public String userId;
    public String username;
    public String title;
    public String message;
    public String category;
    public Integer duration;
    public Long datePost;
    public Long dateExpires;
    public double lat;
    public double lng;
    public boolean markedDone;

    public String getBiKey() {
        return hapId;
    }

    public Hap() {
        this.markedDone = false;
    }

    public Hap(String title, String message, double lat, double lng, String category, int duration) {
        this.title = title;
        this.message = message;
        this.lat = lat;
        this.lng = lng;
        this.markedDone = false;
        this.category = category;
        this.duration = duration;
    }

    public Hap(Map<String, Object> map) {
        this.hapId = (String)map.get("hap_id");
        this.userId = (String)map.get("user_id");
        this.username = (String)map.get("username");
        this.title = (String)map.get("title");
        this.message = (String)map.get("message");
        this.markedDone = false;
        this.category = (String)map.get("category");
        this.duration = (Integer)map.get("duration");

        // dates are in unix seconds
        this.datePost = avoid_y2k38(map.get("date_post"));
        this.dateExpires = avoid_y2k38(map.get("date_expires"));
        //this.datePostDate = new Date(this.datePost * 1000); // example of how to convert

        // location is still an Object[] of doubles
        Object[] latAndLng = (Object[])map.get("location");
        this.lat = (double)latAndLng[0];
        this.lng = (double)latAndLng[1];
    }

    public Date getDatePostDate() {
        return toDate(datePost);
    }

    public Date getDateExpiresDate() {
        return toDate(dateExpires);
    }

    private Date toDate(Long unixTime) {
        return new Date(unixTime * 1000);
    }

    private Long avoid_y2k38(Object o) {
        if (o instanceof Long) {
            return (Long)o;
        }
        else if (o instanceof Integer) {
            return ((Integer)o).longValue();
        }
        else return 0L;
    }
}

