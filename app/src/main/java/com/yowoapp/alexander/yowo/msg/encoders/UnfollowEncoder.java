package com.yowoapp.alexander.yowo.msg.encoders;

import com.yowoapp.alexander.yowo.protocol.Protocol;

import java.nio.ByteBuffer;

/**
 * Msg encoder for unfollowing a user.
 */
public class UnfollowEncoder implements MsgEncodeCallback {
    private String _userId;
    private String _username;

    public UnfollowEncoder(String userId, String username) {
        _userId = userId;
        _username = username;
    }

    @Override
    public ByteBuffer encodeMsg() {
        return Protocol.unfollow(_userId, _username);
    }
}
