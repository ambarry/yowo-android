package com.yowoapp.alexander.yowo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.yowoapp.alexander.yowo.protocol.Hap;
import static com.yowoapp.alexander.yowo.db.DbContract.*; // get constants


/**
 * Helpers for db management and access.
 */
public class DbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Happenings.db";

    private static DbHelper s_Instance;

    private DbHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Gets instance. To be safer, we should pass context and create if
     * null... but this would require context everywhere, which is a pain...
     */
    public static synchronized DbHelper getInstance() {
        return s_Instance;
    }

    /**
     * Must init w/ context.
     */
    public static void initDbHelper(Context context) {
        if (s_Instance == null) {
            s_Instance = new DbHelper(context.getApplicationContext());
        }
    }

    /**
     * Close everything.
     */
    public static void doClose(){
        if (s_Instance != null) {
            s_Instance.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db){

        db.execSQL(SQL_CREATE_HAP_TABLE);
        db.execSQL(SQL_CREATE_FOLLOW_TABLE);
        db.execSQL(SQL_CREATE_LOCATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // can drop Haps at least, let the next server init pull active haps
        // NOTE: this will NOT be true for locations, blocks, etc.
        Log.d("DbHelper", "upgrading...");
        db.execSQL(SQL_DELETE_HAP_TABLE);
        db.execSQL(SQL_DELETE_FOLLOW_TABLE);
        db.execSQL(SQL_DELETE_LOCATION_TABLE);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    /**
     * Checks if Hap has already been stored.
     */
    public boolean hapExists(SQLiteDatabase db, String hapId) {
        String[] columns = new String [] {DbContract.HapTable._ID};
        String where = DbContract.HapTable.COLUMN_NAME_HAP_ID + " = ?";
        String[] args = new String[] {hapId};
        String limit = "1";
        Cursor cursor = db.query(HapTable.TABLE_NAME, columns, where,
                args, null, null, null, limit);
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        return exists;
    }

    /**
     * Checks for location conflict.
     */
    public boolean hapHasLocationConflict(SQLiteDatabase db, double lat, double lng) {
        String[] columns = new String [] {HapTable._ID};
        String where = HapTable.COLUMN_NAME_LAT + " = ? AND " +
                HapTable.COLUMN_NAME_LNG + " = ? AND " +
                HapTable.COLUMN_NAME_MARKED_DONE + " = 0";
        String[] args = new String[] {String.valueOf(lat), String.valueOf(lng)};
        String limit = "1"; // only care if there is a conflict, should only be 1 max anyway
        Cursor cursor = db.query(HapTable.TABLE_NAME, columns, where,
                args, null, null, null, limit);
        boolean hasConflict = (cursor.getCount() > 0);
        cursor.close();
        return hasConflict;
    }

    /**
     * Saves a Hap to db.
     * Checks for existence.
     * Pads location if conflict.
     */
    private Hap insertHap(SQLiteDatabase db, Hap hap, int tries) {
        // if exceed max tries to reposition, error
        if (tries == 5) {
            // TODO: may want to overwrite older one or add to existing marker?
            Log.d("DbHelper", "Maximum tries exceeded. Abandoning hap.");
            return null;
        }


        // only check for existence (duplicate) on first try
        if (tries == 0 && hapExists(db, hap.hapId)) {
            return null;
        }

        // save if all good
        if (!hapHasLocationConflict(db, hap.lat, hap.lng)) {
            ContentValues values = new ContentValues();
            values.put(HapTable.COLUMN_NAME_HAP_ID, hap.hapId);
            values.put(HapTable.COLUMN_NAME_USER_ID, hap.userId);
            values.put(HapTable.COLUMN_NAME_USERNAME, hap.username);
            values.put(HapTable.COLUMN_NAME_TITLE, hap.title);
            values.put(HapTable.COLUMN_NAME_MESSAGE, hap.message);
            values.put(HapTable.COLUMN_NAME_LAT, hap.lat);
            values.put(HapTable.COLUMN_NAME_LNG, hap.lng);
            values.put(HapTable.COLUMN_NAME_DATE_POST, hap.datePost);
            values.put(HapTable.COLUMN_NAME_DATE_EXPIRES, hap.dateExpires);
            values.put(HapTable.COLUMN_NAME_MARKED_DONE, hap.markedDone ? 1 : 0);
            values.put(HapTable.COLUMN_NAME_CATEGORY, hap.category);
            values.put(HapTable.COLUMN_NAME_DURATION, hap.duration);

            db.insert(HapTable.TABLE_NAME, null, values);
            Log.d("DbHelper", "hap saved!");
            return hap;
        }

        // otherwise reposition and try again
        else {
            Log.d("DbHelper", "padding hap for conflict");
            hap.lng += 0.0001;
            tries++;
            return insertHap(db, hap, tries);
        }
    }

    /**
     * Saves a Hap to db.
     * Checks for existence.
     * Pads location if conflict.
     */
    public Hap insertHap(SQLiteDatabase db, Hap hap) {
        return insertHap(db, hap, 0);
    }

    /**
     * Sets the done flag on the hap.
     * Haps that are marked done can be hidden from display.
     */
    public void markHapDone(SQLiteDatabase db, String hapId) {
        ContentValues values = new ContentValues();
        values.put(HapTable.COLUMN_NAME_MARKED_DONE, 1);
        String where = HapTable.COLUMN_NAME_HAP_ID + " = ?";
        String[] args = new String[] {hapId};
        db.update(HapTable.TABLE_NAME, values, where, args);
    }

    /**
     * Deletes expired haps from db.
     */
    public void cleanExpiredHaps(SQLiteDatabase db) {
        // get current unix time utc
        // delete expired haps
        String where = HapTable.COLUMN_NAME_DATE_EXPIRES + " < ?";
        String[] args = new String[] {String.valueOf(getUnixTime())};
        db.delete(HapTable.TABLE_NAME, where, args);
    }

    private long getUnixTime() {
        return System.currentTimeMillis() / 1000;
    }

    /**
     * Gets a Hap from a db row.
     */
    public Hap getHapRow(Cursor cursor) {
        Hap hap = new Hap();
        hap.hapId = cursor.getString(cursor.getColumnIndexOrThrow(HapTable.COLUMN_NAME_HAP_ID));
        hap.userId = cursor.getString(cursor.getColumnIndexOrThrow(HapTable.COLUMN_NAME_USER_ID));
        hap.username = cursor.getString(cursor.getColumnIndexOrThrow(HapTable.COLUMN_NAME_USERNAME));
        hap.title = cursor.getString(cursor.getColumnIndexOrThrow(HapTable.COLUMN_NAME_TITLE));
        hap.message = cursor.getString(cursor.getColumnIndexOrThrow(HapTable.COLUMN_NAME_MESSAGE));
        hap.lat = cursor.getDouble(cursor.getColumnIndexOrThrow(HapTable.COLUMN_NAME_LAT));
        hap.lng = cursor.getDouble(cursor.getColumnIndexOrThrow(HapTable.COLUMN_NAME_LNG));
        hap.datePost = cursor.getLong(cursor.getColumnIndexOrThrow(HapTable.COLUMN_NAME_DATE_POST));
        hap.dateExpires = cursor.getLong(cursor.getColumnIndexOrThrow(HapTable.COLUMN_NAME_DATE_EXPIRES));
        hap.markedDone = cursor.getInt(
                cursor.getColumnIndexOrThrow(HapTable.COLUMN_NAME_MARKED_DONE)) != 0;
        hap.category = cursor.getString(cursor.getColumnIndexOrThrow(HapTable.COLUMN_NAME_CATEGORY));
        hap.duration = cursor.getInt(cursor.getColumnIndexOrThrow(HapTable.COLUMN_NAME_DURATION));
        return hap;
    }

    /**
     * Gets active haps from db.
     * Must not have expired and not be marked done.
     */
    public Hap[] getActiveHaps(SQLiteDatabase db) {
        Log.d("DbHelper", "getting active haps");
        String[] columns = new String [] {
            HapTable.COLUMN_NAME_HAP_ID,
            HapTable.COLUMN_NAME_USER_ID,
            HapTable.COLUMN_NAME_USERNAME,
            HapTable.COLUMN_NAME_TITLE,
            HapTable.COLUMN_NAME_MESSAGE,
            HapTable.COLUMN_NAME_LAT,
            HapTable.COLUMN_NAME_LNG,
            HapTable.COLUMN_NAME_DATE_POST,
            HapTable.COLUMN_NAME_DATE_EXPIRES,
            HapTable.COLUMN_NAME_MARKED_DONE,
            HapTable.COLUMN_NAME_DURATION,
            HapTable.COLUMN_NAME_CATEGORY
        };
        String where = HapTable.COLUMN_NAME_DATE_EXPIRES + " > ? AND " + HapTable.COLUMN_NAME_MARKED_DONE + " = 0";
        String[] args = new String[] {String.valueOf(getUnixTime())};
        Cursor cursor = db.query(HapTable.TABLE_NAME, columns, where, args, null, null, null, null);
        Hap[] haps = new Hap[cursor.getCount()];
        int i = 0;
        while(cursor.moveToNext()) {
            haps[i] = getHapRow(cursor);
            i++;
        }

        cursor.close();
        Log.d("DbHelper", "returning haps");
        return haps;
    }

    /**
     * Follows a user.
     */
    public void follow(SQLiteDatabase db, String userId, String username) {
        if (!isFollowing(db, userId)) {
            ContentValues values = new ContentValues();
            values.put(FollowTable.COLUMN_NAME_USER_ID, userId);
            values.put(FollowTable.COLUMN_NAME_USERNAME, username);
            db.insert(FollowTable.TABLE_NAME, null, values);
        }
    }

    /**
     * Unfollows a user.
     */
    public void unfollow(SQLiteDatabase db, String userId) {
        String where = FollowTable.COLUMN_NAME_USER_ID + " = ?";
        String[] args = new String[] {userId};
        db.delete(FollowTable.TABLE_NAME, where, args);
    }

    /**
     * Determines whether you're following a user.
     */
    public boolean isFollowing(SQLiteDatabase db, String userId) {
        String[] columns = new String[] {FollowTable._ID};
        String where = FollowTable.COLUMN_NAME_USER_ID + " = ?";
        String[] args = new String[]{userId};
        String limit = "1";
        Cursor cursor = db.query(FollowTable.TABLE_NAME, columns, where, args, null, null, null, limit);
        boolean isFollowing = cursor.getCount() > 0;
        cursor.close();
        return isFollowing;
    }

    /**
     * Saves locations.
     */
    public void saveLocation(String name, double lat, double lng) {
        // TODO: going to have to update server for name...

    }
}