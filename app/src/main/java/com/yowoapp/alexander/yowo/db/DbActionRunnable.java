package com.yowoapp.alexander.yowo.db;

import android.os.Process;

/**
 * Runnable for db actions.
 */
public class DbActionRunnable implements Runnable {
    private DbActionCallback _callback;

    public DbActionRunnable(DbActionCallback callback) {
        _callback = callback;
    }

    public void run() {
        android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
        _callback.doAction();
    }
}
