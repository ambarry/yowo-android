package com.yowoapp.alexander.yowo.cnx;

import android.util.Log;

import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

/**
 * Singleton for managing connection across Activities.
 */
public class ConnectionManager {
    // --- instance ---
    private static final ConnectionManager s_Instance = new ConnectionManager();

    // --- socket settings ---
    final String YOWO_IP = "162.243.209.109";
    final int YOWO_PORT = 4040;

    // --- states
    public static final int CONNECTION_COMPLETE = 1;
    public static final int CONNECTION_ERROR = 2;

    // ---
    private Socket _socket;
    private WritableByteChannel _channel;
    private Thread _socketReadLoopThread = null;
    private ConnectionReadCallback _readCallback;
    private ConnectionFailedCallback _connectionFailedCallback;

    /**
     * Private constructor so no other class can instantiate
     */
    private ConnectionManager() {

    }

    /**
     * Gets the ConnectionManager singleton instance.
     */
    public static ConnectionManager getInstance() {
        return s_Instance;
    }

    /**
     * Connects to server in background thread.
     * The ConnectorRunnable will set the socket, channel, and readThread of
     * this ConnectionManager, and start a read loop.
     */
    public void startConnection() {
        ConnectorRunnable cRun = new ConnectorRunnable(YOWO_IP, YOWO_PORT);
        Thread socketThread = new Thread(cRun);
        socketThread.start();
    }

    /**
     * State processor for connection thread.
     */
    public void handleConnectionState(int state) {
        switch(state) {
            case CONNECTION_COMPLETE:
                // don't really need to do anything...
                // at least until we display a waiting UI symbol
                Log.d("[<<< CnxMgr >>>]", "connection complete");
                break;
            case CONNECTION_ERROR:
                Log.d("[<<< CnxMgr >>>]", "connection error");
                if (_connectionFailedCallback != null) {
                    _connectionFailedCallback.handleConnectionFailed();
                }
                break;
            default:
                break;
        }
    }

    /**
     * Sets the callback that all received messages are forwarded through.
     * This can be swapped out depending on the Activity, while
     * keeping the connection going.
     */
    public void setReadCallback(ConnectionReadCallback callback) {
        _readCallback = callback;
    }

    /**
     * Sets the callback to use to handle connection failures.
     * Should really just be the MainActivity.
     * TODO: consider making a more generic ConnectionDisplayStatus callback or something
     * so you can show when trying to connect too.
     */
    public void setConnectionFailedCallback(ConnectionFailedCallback callback) {
        _connectionFailedCallback = callback;
    }

    /**
     * Handles incoming buffer through callback.
     */
    public void handleBuffer(ByteBuffer buffer) {
        _readCallback.handleBuffer(buffer);
    }

    public void setSocket(Socket socket) {
        _socket = socket;
    }

    public void setChannel(WritableByteChannel channel) {
        _channel = channel;
    }

    public void setReadThread(Thread thread) {
        _socketReadLoopThread = thread;
    }

    public WritableByteChannel getChannel() {
        return _channel;
    }


    /**
     * Stops the read loop.
     */
    public static void cancelRead() {
        synchronized (s_Instance) {
            if (s_Instance._socketReadLoopThread != null) {
                s_Instance._socketReadLoopThread.interrupt();
            }
        }
    }

}
