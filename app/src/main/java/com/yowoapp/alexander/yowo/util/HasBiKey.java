package com.yowoapp.alexander.yowo.util;

public interface HasBiKey<T> {
    T getBiKey();
}
