package com.yowoapp.alexander.yowo;

import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.yowoapp.alexander.yowo.protocol.Hap;
import com.yowoapp.alexander.yowo.util.BiKeyMapper;

public class HapInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private final View _view;
    private BiKeyMapper<Marker, String, Hap> _markerHapMap;
    private ImageView imgProfilePic;
    private TextView _txtTitle;
    private TextView _txtBody;
    private TextView _txtUserName;
    private TextView _txtDatePost;
    private TextView _txtDateExpires;
    private TextView _txtCategory;

    public HapInfoWindowAdapter(LayoutInflater inflater, BiKeyMapper<Marker, String, Hap> markerHapMap) {
        _markerHapMap = markerHapMap;
        _view = inflater.inflate(R.layout.hap_info_window, null);
        imgProfilePic = (ImageView) _view.findViewById(R.id.imgProfilePic);
        _txtTitle = (TextView)_view.findViewById(R.id.txtHapMarkerTitle);
        _txtBody = (TextView)_view.findViewById(R.id.txtHapMarkerBody);
        _txtUserName = (TextView)_view.findViewById(R.id.txtUserName);
        _txtDatePost = (TextView)_view.findViewById(R.id.txtDatePost);
        _txtDateExpires = (TextView)_view.findViewById(R.id.txtDateExpires);
        _txtCategory = (TextView)_view.findViewById(R.id.txtCategory);
    }

    @Override
    public View getInfoWindow(Marker marker){
        // called first, allows total customization of entire view, not just contents...
        //return null;

        Hap hap = _markerHapMap.get(marker);
        _txtTitle.setText(hap.title);
        _txtBody.setText(hap.message);
        _txtUserName.setText(hap.username);
        _txtCategory.setText(hap.category);
        String posted = "Posted: " + hap.getDatePostDate().toString();
        String expires = "Expires: " + hap.getDateExpiresDate().toString();
        _txtDatePost.setText(posted);
        _txtDateExpires.setText(expires);

        // TODO: handle pic more appropriately from hap -> may need to retrieve here and cache, unless already done
        Drawable drw = ResourcesCompat.getDrawable(_view.getContext().getResources(), R.drawable.brak, null);
        imgProfilePic.setImageDrawable(drw);

        return _view;
    }

    @Override
    public View getInfoContents(Marker marker) {
        // allows customization of contents but uses default view
        //only runs if getInfoWindow() returns null

        Hap hap = _markerHapMap.get(marker);
        _txtTitle.setText(hap.title);
        _txtBody.setText(hap.message);
        _txtUserName.setText(hap.username);

        // TODO: handle pic more appropriately from hap -> may need to retrieve here and cache, unless already done
        Drawable drw = ResourcesCompat.getDrawable(_view.getContext().getResources(), R.drawable.brak, null);
        imgProfilePic.setImageDrawable(drw);

        return _view;
    }
}

