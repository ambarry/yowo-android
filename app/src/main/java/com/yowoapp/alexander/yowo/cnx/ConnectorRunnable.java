package com.yowoapp.alexander.yowo.cnx;

import android.util.Log;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;

/**
 * Establishes connection to server.
 * Sets the socket and channel.
 * Starts read thread.
 */
public class ConnectorRunnable implements Runnable {
    private String _ip;
    private int _port;

    public ConnectorRunnable(String ip, int port)
    {
        _ip = ip;
        _port = port;
    }

    @Override
    public void run() {
        ConnectionManager cnx = ConnectionManager.getInstance();
        try {
            InetAddress address = getYowoInetAddress(_ip);
            if (address == null){
                cnx.handleConnectionState(ConnectionManager.CONNECTION_ERROR);
                return;
            }

            // create socket
            Socket sock = new Socket(address, _port);
            Log.d("[<<< ConnectorRun >>>]", "socket connected!!!");

            // make sure we start the read loop first, as each write can end and we don't want the socket to close...
            ConnectionReaderRunnable readRunnable = new ConnectionReaderRunnable(sock);
            Thread readThread = new Thread(readRunnable);
            readThread.start();

            // create write channel
            WritableByteChannel channel = Channels.newChannel(sock.getOutputStream());

            // set connection manager
            cnx.setReadThread(readThread);
            cnx.setChannel(channel);
            cnx.setSocket(sock);

            // complete
            cnx.handleConnectionState(ConnectionManager.CONNECTION_COMPLETE);

        } catch (IOException ex) {
            cnx.handleConnectionState(ConnectionManager.CONNECTION_ERROR);
            Log.d("[<<< ConnectorRun >>>]", "IO Exception from Socket!\n" + ex.toString());
        }
    }

    private InetAddress getYowoInetAddress(String ip) {
        InetAddress addr = null;
        try {
            InetAddress[] addresses = InetAddress.getAllByName(ip);
            if (addresses.length > 0) {
                addr = addresses[0];
            }
        } catch (UnknownHostException uex) {
            Log.d("[<<< ConnectorRun >>>]", uex.getMessage());
        }
        return addr;
    }
}