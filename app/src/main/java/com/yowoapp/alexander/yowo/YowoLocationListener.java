package com.yowoapp.alexander.yowo;


import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

public class YowoLocationListener implements LocationListener {
    @Override
    public void onLocationChanged(Location location) {
        // this is where we'd check and post :subscribe
        Log.d("Location", "Location update");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
