package com.yowoapp.alexander.yowo.db;

/**
 * Action to unfollow user from db.
 */
public class UnfollowUserAction implements DbActionCallback {
    private String _userId;
    public UnfollowUserAction(String userId){
        _userId = userId;
    }

    @Override
    public void doAction() {
        DbHelper dbHelper = DbHelper.getInstance();
        dbHelper.unfollow(dbHelper.getWritableDatabase(), _userId);
    }
}
