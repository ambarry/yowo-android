package com.yowoapp.alexander.yowo.msg.encoders;

import com.yowoapp.alexander.yowo.protocol.Protocol;

import java.nio.ByteBuffer;

/**
 * Creates msg encode callback for
 * initializing a user after authentication.
 */
public class InitUserEncoder implements MsgEncodeCallback {
    public InitUserEncoder() {}

    @Override
    public ByteBuffer encodeMsg() {
        return Protocol.init_user();
    }
}
