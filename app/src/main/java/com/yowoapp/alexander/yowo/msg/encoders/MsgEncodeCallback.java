package com.yowoapp.alexander.yowo.msg.encoders;

import java.nio.ByteBuffer;

/**
 * Callback interface for message encoding.
 * Accepts a write task and gets appropriate values
 * depending on type of message.
 */
public interface MsgEncodeCallback {
    ByteBuffer encodeMsg();
}
