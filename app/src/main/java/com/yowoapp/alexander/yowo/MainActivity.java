package com.yowoapp.alexander.yowo;

import android.app.Activity;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.yowoapp.alexander.yowo.cnx.ConnectionFailedCallback;
import com.yowoapp.alexander.yowo.cnx.ConnectionManager;
import com.yowoapp.alexander.yowo.db.CleanHapsAction;
import com.yowoapp.alexander.yowo.db.DbActionRunnable;
import com.yowoapp.alexander.yowo.db.DbHelper;

/**
 * The main activity. Mostly handles navigation and the app bar updates.
 */
public class MainActivity extends AppCompatActivity implements
    NavigationView.OnNavigationItemSelectedListener,
    YowoMapFragment.OnYowoMapFragmentListener,
    CloseTheDamnDrawer.OnDamnDrawerClosed,
    ConnectionFailedCallback,
    LoginFragment.OnLoginFragmentListener {

    private DrawerLayout _drawer;
    private boolean _needToLoadFirstMap;
    private boolean _navToMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set tool bar
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        // toggle action bar on drawer slide
        _drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        // ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, _drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        CloseTheDamnDrawer toggle = new CloseTheDamnDrawer(this, _drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close, this);
        _drawer.addDrawerListener(toggle); // note: setDrawerListener (from examples) is deprecated
        toggle.syncState();


        // handle nav drawer clicks (use setNavigationItemSelectedListener
        // instead of ListView.OnItemClickListener from DrawerLayout,
        // since built-in to NavView
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        _navToMap = false;
        _needToLoadFirstMap = true; // TODO: check placement / lifecycle of this

        // init db helper
        DbHelper.initDbHelper(getApplicationContext());

        // clean haps
        new Thread(new DbActionRunnable(new CleanHapsAction())).start();

        // set up alert callback
        ConnectionManager.getInstance().setConnectionFailedCallback(this);

        // TODO: load login frag, let it try to connect first, if yes, swap frags, otherwise keep
        // set default fragment
        if (savedInstanceState == null)
        {
            Log.d("[<<< MainActivity >>>]","setting default fragment");
            //setFragment(TestFragment.newInstance("main activity default"));
            setFragment(LoginFragment.newInstance());
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        getActionBar().setTitle(title);
    }

    @Override
    public void onBackPressed() {
        if (_drawer.isDrawerOpen(GravityCompat.START)) {
            _drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                return true;
            case R.id.action_add_hap:
                return false; // implemented in fragment
            case R.id.action_connect:
                return false; // implemented in fragment
            default:
                return false;
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment = null;
        int id = item.getItemId();
        switch(id) {
            case R.id.nav_map:
                if (!_needToLoadFirstMap) {
                    fragment = YowoMapFragment.newInstance();
                }
                else {
                    _navToMap = true;
                }
                break;
            default:
                fragment = TestFragment.newInstance("menu");
                break;
        }

        if (fragment != null) {
            Log.d("[<<< MainActivity >>>]","switching frags");
            setFragment(fragment);
        }

        Log.d("[<<< MainActivity >>>]","closing drawer");
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDamnDrawerClosed(View drawerView) {
        Log.d("[<<< MainActivity >>>]", "the damn drawer is closed");
        if (_navToMap) {
            _navToMap = false;
            _needToLoadFirstMap = false;
            setFragment(YowoMapFragment.newInstance());
        }
    }

    private void setFragment(Fragment fragment) {

        // Insert the fragment by replacing any existing fragment
        // note: let the fragment set the toolbar and title?
        getSupportFragmentManager().beginTransaction()
        .replace(R.id.content_frame, fragment)
        .commit();
    }

    @Override
    public void onTitleChange(String title) {
        ActionBar actionBar = getSupportActionBar(); // must use SupportActionBar, as ActionBar may be null
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    @Override
    public void onLogIn() {

    }

    /**
     * Close db connection on stop.
     */
    @Override
    protected void onStop() {
        DbHelper.doClose();
        ConnectionManager.cancelRead();
        super.onStop();
    }

    /**
     * Shows a dialog when connection fails.
     */
    @Override
    public void handleConnectionFailed() {
        alert("Connection Error", "Whoops! We couldn't connect to the server." +
                " Make sure you have a connection going if you want the best experience.");
    }

    /**
     * Shows a simple alert mesasge.
     */
    public void alert(String title, String msg) {
        runOnUiThread(new AlertRunnable(this, title, msg));
    }

}
