package com.yowoapp.alexander.yowo.db;

import android.provider.BaseColumns;


/**
 * Maintains contract for schema.
 * Tables should be represented by inner classes.
 * Should not instantiate.
 */
public final class DbContract {
    public DbContract(){}

    // saving spaces and typos...
    private static final String INTEGER_TYPE = " INTEGER"; // note: SQLite ints store longs too
    private static final String REAL_TYPE = " REAL";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    /**
     * Schema for Hap Table.
     * Sets up column contract.
     * NOTE: inheriting BaseColumns adds an _ID field which we can
     * store as the pk int.
     * This field makes working w/ some Android classes like cursor adapters
     * a little smoother supposedly...
     */
    public static abstract class HapTable implements BaseColumns {
        public static final String TABLE_NAME = "haps";
        public static final String COLUMN_NAME_HAP_ID = "hapId";
        public static final String COLUMN_NAME_USER_ID = "userId";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_MESSAGE = "message";
        public static final String COLUMN_NAME_LAT = "lat";
        public static final String COLUMN_NAME_LNG = "lng";
        public static final String COLUMN_NAME_DATE_POST = "datePost";
        public static final String COLUMN_NAME_DATE_EXPIRES = "dateExpires";
        public static final String COLUMN_NAME_MARKED_DONE = "markedDone";
        public static final String COLUMN_NAME_CATEGORY = "category";
        public static final String COLUMN_NAME_DURATION = "duration";
    }

    /**
     * Schema for Follow Table.
     */
    public static abstract class FollowTable implements BaseColumns {
        public static final String TABLE_NAME = "follows";
        public static final String COLUMN_NAME_USER_ID = "userId";
        public static final String COLUMN_NAME_USERNAME = "username";
    }

    /**
     * Schema for Location Table.
     */
    public static abstract class LocationTable implements BaseColumns {
        public static final String TABLE_NAME = "locations";
        public static final String COLUMN_NAME_LOCATION_NAME = "locationName";
        public static final String COLUMN_NAME_LAT = "lat";
        public static final String COLUMN_NAME_LNG = "lng";
    }


    /**
     * Create SQL Hap Table.
     */
    public static final String SQL_CREATE_HAP_TABLE =
        "CREATE TABLE " + HapTable.TABLE_NAME + " (" +
                HapTable._ID + " INTEGER PRIMARY KEY," +
                HapTable.COLUMN_NAME_HAP_ID + TEXT_TYPE + COMMA_SEP +
                HapTable.COLUMN_NAME_USER_ID + TEXT_TYPE + COMMA_SEP +
                HapTable.COLUMN_NAME_USERNAME + TEXT_TYPE + COMMA_SEP +
                HapTable.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                HapTable.COLUMN_NAME_MESSAGE + TEXT_TYPE + COMMA_SEP +
                HapTable.COLUMN_NAME_LAT + REAL_TYPE + COMMA_SEP +
                HapTable.COLUMN_NAME_LNG + REAL_TYPE + COMMA_SEP +
                HapTable.COLUMN_NAME_DATE_POST + INTEGER_TYPE + COMMA_SEP +
                HapTable.COLUMN_NAME_DATE_EXPIRES + INTEGER_TYPE + COMMA_SEP +
                HapTable.COLUMN_NAME_MARKED_DONE + INTEGER_TYPE + COMMA_SEP +
                HapTable.COLUMN_NAME_CATEGORY + TEXT_TYPE + COMMA_SEP +
                HapTable.COLUMN_NAME_DURATION + INTEGER_TYPE +
                ")";

    /**
     * Drop SQL Hap Table.
     */
    public static final String SQL_DELETE_HAP_TABLE =
        "DROP TABLE IF EXISTS " + HapTable.TABLE_NAME;

    /**
     * Create SQL Follow Table.
     */
    public static final String SQL_CREATE_FOLLOW_TABLE =
        "CREATE TABLE " + FollowTable.TABLE_NAME + " (" +
                FollowTable._ID + " INTEGER PRIMARY KEY," +
                FollowTable.COLUMN_NAME_USER_ID + TEXT_TYPE + COMMA_SEP +
                FollowTable.COLUMN_NAME_USERNAME + TEXT_TYPE +
                ")";
    /**
     * Drop SQL Follow Table.
     */
    public static final String SQL_DELETE_FOLLOW_TABLE =
        "DROP TABLE IF EXISTS "+ FollowTable.TABLE_NAME;

    /**
     * Create SQL Location Table.
     */
    public static final String SQL_CREATE_LOCATION_TABLE =
        "CREATE TABLE " + LocationTable.TABLE_NAME + " (" +
                LocationTable._ID + " INTEGER PRIMARY KEY," +
                LocationTable.COLUMN_NAME_LOCATION_NAME + TEXT_TYPE + COMMA_SEP +
                LocationTable.COLUMN_NAME_LAT + REAL_TYPE + COMMA_SEP +
                LocationTable.COLUMN_NAME_LNG + REAL_TYPE +
                ")";

    public static final String SQL_DELETE_LOCATION_TABLE =
        "DROP TABLE IF EXISTS "+ LocationTable.TABLE_NAME;
}
