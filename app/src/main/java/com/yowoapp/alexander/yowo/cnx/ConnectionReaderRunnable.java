package com.yowoapp.alexander.yowo.cnx;

import android.util.Log;

import java.io.DataInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.nio.ByteBuffer;

/**
 * Listens to socket for incoming messages.
 */
public class ConnectionReaderRunnable implements Runnable {
    private WeakReference<Socket> _socketRef;

    public ConnectionReaderRunnable(Socket socket) {
        _socketRef = new WeakReference<>(socket);
    }

    @Override
    public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
        loopRead();
    }

    private void loopRead() {
        if (_socketRef == null) {
            return;
        }

        try (DataInputStream iStream = new DataInputStream(_socketRef.get().getInputStream())){
            int len;
            while(true) {

                // check thread interrupt for cancellation
                if (Thread.interrupted()) {
                    return;
                }

                len = iStream.readInt(); // packet 4 means Elixir will always send 4 byte length header
                Log.d("Length", Integer.toString(len));
                byte[] bytes = new byte[len];
                iStream.read(bytes, 0, len); // know len, can ignore return (num bytes read)
                ByteBuffer buf = ByteBuffer.wrap(bytes);
                ConnectionManager.getInstance().handleBuffer(buf);
            }
        } catch (IOException iex) {
            Log.d("Reader", iex.toString());
        }
    }
}
