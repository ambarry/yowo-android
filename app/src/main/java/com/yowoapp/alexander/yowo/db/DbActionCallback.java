package com.yowoapp.alexander.yowo.db;

/**
 * Simple interface for performing one-off db actions.
 * To be used in runnable in bg.
 */
public interface DbActionCallback {
    void doAction();
}
