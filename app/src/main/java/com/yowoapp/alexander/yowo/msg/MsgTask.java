package com.yowoapp.alexander.yowo.msg;

/**
 * Base class for msg writing and reading tasks.
 * All tasks will need ref to the static MsgManager,
 * as well as the ability to get/set Runnable thread,
 * and recycle.
 */
abstract class MsgTask {
    protected static final MsgManager s_MsgManager = MsgManager.getInstance();
    protected Thread runThread;
    protected Runnable runnable;

    /**
     * Sends task (this) and state code to MsgManager for handling.
     */
    protected abstract void handleState(int state);


    /**
     * Returns the runnable.
     */
    public Runnable getMsgRunnable() {
        return runnable;
    }

    /**
     * Returns the Thread that this Task is running on. The method must first get a lock on a
     * static field, in this case the ThreadPool singleton. The lock is needed because the
     * Thread object reference is stored in the Thread object itself, and that object can be
     * changed by processes outside of this app.
     */
    public Thread getCurrentThread() {
        synchronized(s_MsgManager) {
            return runThread;
        }
    }

    /**
     * Sets the identifier for the current Thread. This must be a synchronized operation; see the
     * notes for getCurrentThread()
     */
    public void setCurrentThread(Thread thread) {
        synchronized(s_MsgManager) {
            runThread = thread;
        }
    }

    /**
     * Recycles a Task object before it's put back into the pool. One reason to do
     * this is to avoid memory leaks.
     */
    public abstract void recycle();
}
