package com.yowoapp.alexander.yowo.cnx;

public interface ConnectionFailedCallback {
    void handleConnectionFailed();
}
