package com.yowoapp.alexander.yowo.protocol;

import com.google.android.gms.maps.model.LatLng;

import java.nio.ByteBuffer;


/**
 * Creates the actual data and packets
 * to send to the server, formatted specifically
 * for Yowo terms.
 * It may make extensive use of the ETF functions.
 */
public class Protocol {

    public static ByteBuffer arrive(LatLng latLng)
    {
        return createPacket(Command.ARRIVE, createLocationData(latLng));
    }

    public static ByteBuffer authenticate(String username, String secret)
    {
        return createPacket(Command.AUTHENTICATE, createAuthenticationData(username, secret));
    }

    public static ByteBuffer init_user() {
        return createPacket(Command.INIT_USER, ETF.toETFAtom("ok"));
    }

    public static ByteBuffer publishHap(Hap hap)
    {
        return createPacket(Command.PUBLISH_HAP, createHapData(hap));
    }

    public static ByteBuffer saveLocation(LatLng latLng) {
        return createPacket(Command.SAVE_LOCATION, createLocationData(latLng));
    }

    public static ByteBuffer removeLocation(LatLng latLng) {
        return createPacket(Command.REMOVE_LOCATION, createLocationData(latLng));
    }

    public static ByteBuffer follow(String userId, String username) {
        return createPacket(Command.FOLLOW, createFollowData(userId, username));
    }

    public static ByteBuffer unfollow(String userId, String username) {
        return createPacket(Command.UNFOLLOW, createFollowData(userId, username));
    }

    /**
     * Wraps data in command packet to send to server.
     */
    private static ByteBuffer createPacket(Command command, ByteBuffer data) {
        ByteBuffer buffer = ByteBuffer
                .allocate(2000)
                .put(ETF.toETFAtom("__struct__"))
                .put(ETF.toETFAtom("Elixir.Yowo.Packet"))
                .put(ETF.toETFAtom("command"))
                .put(ETF.toETFBinary(command.toString()))
                .put(ETF.toETFAtom("data"))
                .put(data);
        buffer.flip();
        return ETF.toETFMap(3, buffer);
    }

    /**
     * Creates the data for publishing a new Hap.
     */
    private static ByteBuffer createHapData(Hap hap) {
        // TODO: guarantee we won't overflow the buffer - keys are around 58 bytes at the moment, then vals
        // TODO: improve this -> switch not really necessary, could do sub buffers and add lengths
        // NOTE: dates, hap_id, and user info are set by server, as are generated topics
        ByteBuffer buffer = ByteBuffer.allocate(2000);
        String[] keys = new String[] {"title", "message", "location", "category", "duration"};

        for(String k : keys) {
            buffer.put(ETF.toETFAtom(k));
            switch(k) {
                case "title":
                    buffer.put(ETF.toETFBinary(hap.title));
                    break;
                case "message":
                    buffer.put(ETF.toETFBinary(hap.message));
                    break;
                case "location":
                    buffer.put(toETFSmallTuple(hap.lat, hap.lng));
                    break;
                case "category":
                    buffer.put(ETF.toETFAtom(hap.category));
                    break;
                case "duration":
                    buffer.put(ETF.toETFInt(hap.duration));
                    break;
                default:
                    break;
            }
        }
        buffer.flip();
        return ETF.toETFMap(keys.length, buffer);
    }


    private static ByteBuffer toETFSmallTuple(LatLng latLng) {
        return toETFSmallTuple(latLng.latitude, latLng.longitude);
    }

    private static ByteBuffer toETFSmallTuple(double lat, double lng) {
        // elems are 2 floats -> 9 bytes each
        ByteBuffer buf = ByteBuffer
                .allocate(18)
                .put(ETF.toETFFloat(lat))
                .put(ETF.toETFFloat(lng));
        buf.flip();
        return ETF.toETFSmallTuple(2, buf);
    }

    /**
     * Creates location data, e.g. an ETF map with %{:location => {lat, lng}}
     */
    private static ByteBuffer createLocationData(LatLng latLng) {
        ByteBuffer key = ETF.toETFAtom("location");
        ByteBuffer val = toETFSmallTuple(latLng);
        ByteBuffer kvs = ByteBuffer
                .allocate(key.capacity() + val.capacity())
                .put(key)
                .put(val);
        kvs.flip();
        return ETF.toETFMap(1, kvs);
    }

    /**
     * Creates follow data, e.g. %{:follow_id => userId, :follow_name => username}
     */
    private static ByteBuffer createFollowData(String userId, String username) {
        ByteBuffer buf = ByteBuffer
                .allocate(2000)
                .put(ETF.toETFAtom("follow_id"))
                .put(ETF.toETFBinary(userId))
                .put(ETF.toETFAtom("follow_name"))
                .put(ETF.toETFBinary(username));
        buf.flip();
        return ETF.toETFMap(2, buf);
    }

    /**
     * Creates authentication data, e.g. %{:username => username, :secret => secret}
     */
    private static ByteBuffer createAuthenticationData(String username, String secret){
        ByteBuffer buf = ByteBuffer
                .allocate(1500)
                .put(ETF.toETFAtom("username"))
                .put(ETF.toETFBinary(username))
                .put(ETF.toETFAtom("secret"))
                .put(ETF.toETFBinary(secret));
        buf.flip();
        return ETF.toETFMap(2, buf);
    }

}
