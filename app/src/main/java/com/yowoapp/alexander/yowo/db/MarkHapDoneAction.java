package com.yowoapp.alexander.yowo.db;

/**
 * Marks a Hap done.
 */
public class MarkHapDoneAction implements DbActionCallback {
    private String _hapId;

    public MarkHapDoneAction(String hapId) {
        _hapId = hapId;
    }

    @Override
    public void doAction() {
        DbHelper dbHelper = DbHelper.getInstance();
        dbHelper.markHapDone(dbHelper.getWritableDatabase(), _hapId);
    }
}
