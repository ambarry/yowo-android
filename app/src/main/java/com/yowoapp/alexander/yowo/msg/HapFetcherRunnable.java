package com.yowoapp.alexander.yowo.msg;

import android.os.Process;
import android.util.Log;

import com.yowoapp.alexander.yowo.db.DbHelper;
import com.yowoapp.alexander.yowo.protocol.Hap;

/**
 * Loads map w/ initial haps.
 */
public class HapFetcherRunnable implements Runnable {
    private HapFetcherTask _task;

    public HapFetcherRunnable(HapFetcherTask task) {
        _task = task;
    }

    @Override
    public void run(){
        Log.d("HapFetcherRunnable", "starting...");
        android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
        if (Thread.interrupted()) {
            return;
        }

        // fetch active haps in bg
        Log.d("HapFetcherRunnable", "about to create helper");
        DbHelper s_dbHelper = DbHelper.getInstance();
        Log.d("HapFetcherRunnable", "created helper");
        Hap[] haps = s_dbHelper.getActiveHaps(s_dbHelper.getReadableDatabase());
        Log.d("HapFetcherRunnable", "queried haps");

        // update map on ui thread
        int state = MsgState.NO_INITIAL_HAPS;
        if (haps != null && haps.length > 0) {
            Log.d("HapFetcherRunnable", "found haps...");
            _task.setHaps(haps);
            state = MsgState.FETCHED_INITIAL_HAPS;
        }

        _task.handleState(state);
    }
}
