package com.yowoapp.alexander.yowo.protocol;


public class HapCategory {
    public static String general = "general";
    public static String freeStuff = "free_stuff";
    public static String bizarre = "bizarre";
    public static String food = "food";
    public static String needHelp = "needHelp";
    public static String fubar = "fubar";
}
