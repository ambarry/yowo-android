package com.yowoapp.alexander.yowo.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Bare bones com.yowoapp.alexander.yowo.BiMap.
 * Really just a wrapper to keep 2 maps in sync
 * and lookup from either direction.
 * However, the reverse map may use a diff key than the object itself.
 * Use with caution: values should be unique or you'll have issues...
 */
public class BiKeyMapper<K, Vk, V extends HasBiKey<Vk>> {
    private Map<K, V> _forward;
    private Map<Vk, K> _backward;

    public BiKeyMapper() {
        _forward = new HashMap<K, V>();
        _backward = new HashMap<Vk, K>();
    }

    public void put(K key, Vk valueKey, V value) {
        _forward.put(key, value);
        _backward.put(valueKey, key);
    }

    public V get(K key) {
        return _forward.get(key);
    }

    public K getByValueKey(Vk valueKey) {
        return _backward.get(valueKey);
    }

    public V getValueByValueKey(Vk valueKey) {
        return _forward.get(_backward.get(valueKey));
    }

    public void remove(K key) {
        V val = _forward.remove(key);
        Vk valKey = val.getBiKey();
        _backward.remove(valKey);
    }

    public void removeByValueKey(Vk valueKey) {
        K key = _backward.remove(valueKey);
        _forward.remove(key);
    }

    public void clear() {
        _forward.clear();
        _backward.clear();
    }

    public boolean containsKey(K key){
        return _forward.containsKey(key);
    }

    public boolean containsValueKey(Vk valueKey) {
        return _backward.containsKey(valueKey);
    }
}